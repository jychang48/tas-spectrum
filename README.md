# README #
INSTRUCTIONS ON WRITING THIS PAPER:

Place all figures in file called Figures.tex

Citations: Use Google scholar's Chicago style format

Labels for equations: \label{Eqn:<text>}
Labels for Figures: \label{Fig:<text>}
Labels for Tables: \label{Fig:<text>}
Labels for Algorithms: \label{Alg:<text>}

Upload all source code and matplotlib scripts
