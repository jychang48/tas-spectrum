static char help[] = "Poisson Problem in 3D with tetrahedral and\n\
hexahedral finite elements on a parallel unstructured mesh\n\
(DMPLEX) to discretize it. This example is meant to be run in\n\
a massively parallel environment\n\n\n";

/* Options to run


mpirun -n ... ./Test4 -petscspace_order <1/2> -mat_petscspace_order <1/2> -simplex <0/1> -amp ... -lam .../.../... -cells .../.../... -dm_refine <0,1,2,3>

*/

#include <petscdmplex.h>
#include <petscsnes.h>
#include <petscds.h>
#include <petsctime.h>

typedef struct {
  PetscLogEvent  createMeshEvent;
  
  /* Domain and mesh definition */
  PetscInt       cells[3];          /* The initial domain division */
  PetscReal      amp;               /* The amplitude */
  PetscReal      lam[3];            /* The wavelengths in each dimension */
  PetscBool      simplex;           /* Simplicial mesh */
  
  /* Problem definition */
  PetscErrorCode (**exactFuncs)(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nc, PetscScalar *u, void *ctx);
  
} AppCtx;

static PetscErrorCode zero(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nc, PetscScalar *u, void *ctx)
{
  u[0] = 0.0;
  return 0;
}

static PetscErrorCode forcing(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nc, PetscScalar *u, void *ctx)
{
  AppCtx *user = (AppCtx *) ctx;
  u[0] = user->amp*PetscSqr(PETSC_PI)*(PetscSqr(user->lam[0])+PetscSqr(user->lam[1])+PetscSqr(user->lam[2]))*PetscSinReal(user->lam[0]*PETSC_PI*x[0])*PetscSinReal(user->lam[1]*PETSC_PI*x[1])*PetscSinReal(user->lam[2]*PETSC_PI*x[2]);
  return 0;
}

static void f0_u(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                 const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                 const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                 PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar f0[])
{
  f0[0] = -a[0];
}

/* gradU[comp*dim+d] = {u_x, u_y} or {u_x, u_y, u_z} */
static void f1_u(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                 const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                 const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                 PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar f1[])
{
  PetscInt d;
  for (d = 0; d < dim; ++d) f1[d] = u_x[d];
}

/* < \nabla v, \nabla u + {\nabla u}^T >
   This just gives \nabla u, give the perdiagonal for the transpose */
static void g3_uu(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                  const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                  const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                  PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g3[])
{
  PetscInt d;
  for (d = 0; d < dim; ++d) g3[d*dim+d] = 1.0;
}

static PetscErrorCode u_exact_3d(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nc, PetscScalar *u, void *ctx)
{
  AppCtx *user = (AppCtx *) ctx;
  *u = user->amp*PetscSinReal(user->lam[0]*PETSC_PI*x[0])*PetscSinReal(user->lam[1]*PETSC_PI*x[1])*PetscSinReal(user->lam[2]*PETSC_PI*x[2]);
  //*u = PetscSinReal(2*PETSC_PI*x[0])*PetscSinReal(2*PETSC_PI*x[1])*PetscSinReal(2*PETSC_PI*x[2]);
  return 0;
}

static PetscErrorCode ProcessOptions(MPI_Comm comm, AppCtx *options)
{
  PetscInt       n;
  //PetscBool      flg;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  options->cells[0]            = 2;
  options->cells[1]            = 2;
  options->cells[2]            = 2;
  options->amp                 = 1.0;
  options->lam[0]              = 2.0;
  options->lam[1]              = 2.0;
  options->lam[2]              = 2.0;
  options->simplex             = PETSC_TRUE;

  ierr = PetscOptionsBegin(comm, "", "Poisson Problem Options", "DMPLEX");CHKERRQ(ierr);
  
  n = 3;
  ierr = PetscOptionsIntArray("-cells", "The initial mesh division", "ex12.c", options->cells, &n, NULL);CHKERRQ(ierr);
  
  n = 3;
  ierr = PetscOptionsRealArray("-lam", "The xyz wavelengths", "ex12.c", options->lam, &n, NULL);CHKERRQ(ierr);

  
  ierr = PetscOptionsBool("-simplex", "Simplicial (true) or tensor (false) mesh", "ex12.c", options->simplex, &options->simplex, NULL);CHKERRQ(ierr);
  
  ierr = PetscOptionsReal("-amp", "Amplitude of solution", "ex12.c", options->amp, &options->amp, NULL);CHKERRQ(ierr);
  
  ierr = PetscOptionsEnd();
  ierr = PetscLogEventRegister("CreateMesh", DM_CLASSID, &options->createMeshEvent);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode CreateBCLabel(DM dm, const char name[])
{
  DMLabel        label;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMCreateLabel(dm, name);CHKERRQ(ierr);
  ierr = DMGetLabel(dm, name, &label);CHKERRQ(ierr);
  ierr = DMPlexMarkBoundaryFaces(dm, label);CHKERRQ(ierr);
  ierr = DMPlexLabelComplete(dm, label);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode CreateMesh(MPI_Comm comm, AppCtx *user, DM *dm)
{
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscLogEventBegin(user->createMeshEvent,0,0,0,0);CHKERRQ(ierr);

  ierr = DMPlexCreateBoxMesh(comm, 3, user->simplex, user->cells, NULL, NULL, NULL, PETSC_TRUE, dm);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) *dm, "Mesh");CHKERRQ(ierr);
  {
    PetscPartitioner part;
    DM               distributedMesh = NULL;

    /* Distribute mesh over processes */
    ierr = DMPlexGetPartitioner(*dm,&part);CHKERRQ(ierr);
    ierr = PetscPartitionerSetFromOptions(part);CHKERRQ(ierr);
    ierr = DMPlexDistribute(*dm, 0, NULL, &distributedMesh);CHKERRQ(ierr);
    if (distributedMesh) {
      ierr = DMDestroy(dm);CHKERRQ(ierr);
      *dm  = distributedMesh;
    }
  }
  
  /* Check for markers */
  PetscBool hasLabel;
  if (user->simplex)  {
    DMLabel label;
    ierr = DMRemoveLabel(*dm, "marker", &label);CHKERRQ(ierr);
    ierr = DMLabelDestroy(&label);CHKERRQ(ierr);
  }
  ierr = DMHasLabel(*dm,"marker",&hasLabel);CHKERRQ(ierr);
  if (!hasLabel) {ierr = CreateBCLabel(*dm, "marker");CHKERRQ(ierr);}
  
  ierr = DMLocalizeCoordinates(*dm);CHKERRQ(ierr); /* needed for periodic */
  ierr = DMSetFromOptions(*dm);CHKERRQ(ierr);
  ierr = DMViewFromOptions(*dm, NULL, "-dm_view");CHKERRQ(ierr);
  ierr = PetscLogEventEnd(user->createMeshEvent,0,0,0,0);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode SetupProblem(PetscDS prob, AppCtx *user)
{
  const PetscInt id = 1;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscDSSetResidual(prob, 0, f0_u, f1_u);CHKERRQ(ierr);
  ierr = PetscDSSetJacobian(prob, 0, 0, NULL, NULL, NULL, g3_uu);CHKERRQ(ierr);
  user->exactFuncs[0]  = u_exact_3d;
  ierr = PetscDSAddBoundary(prob, DM_BC_ESSENTIAL, "wall", "marker", 0, 0, NULL, (void (*)()) user->exactFuncs[0], 1, &id, user);CHKERRQ(ierr);
  ierr = PetscDSSetExactSolution(prob, 0, user->exactFuncs[0]);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode SetupDiscretization(DM dm, AppCtx *user)
{
  DM             cdm   = dm;
  DM             dmAux, coordDM;
  PetscFE        fe, feAux = NULL;
  PetscDS        prob, probAux = NULL;
  PetscBool      simplex = user->simplex;
  PetscErrorCode ierr;
  PetscErrorCode (*matFuncs[1])(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nc, PetscScalar u[], void *ctx) = {forcing};
  void           *ctx[1];
  Vec            f;

  PetscFunctionBeginUser;
  /* Create finite element */
  ctx[0] = user;
  ierr = PetscFECreateDefault(dm, 3, 1, simplex, NULL, -1, &fe);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) fe, "potential");CHKERRQ(ierr);

  /* Create auxilliary finite element */
  PetscQuadrature q;
  ierr = PetscFECreateDefault(dm, 3, 1, simplex, "mat_", -1, &feAux);CHKERRQ(ierr);
  ierr = PetscFEGetQuadrature(fe, &q);CHKERRQ(ierr);
  ierr = PetscFESetQuadrature(feAux, q);CHKERRQ(ierr);
  ierr = PetscDSCreate(PetscObjectComm((PetscObject)dm),&probAux);CHKERRQ(ierr);
  ierr = PetscDSSetDiscretization(probAux, 0, (PetscObject) feAux);CHKERRQ(ierr);

  /* Set discretization and boundary conditions for each mesh */
  ierr = DMGetDS(dm, &prob);CHKERRQ(ierr);
  ierr = PetscDSSetDiscretization(prob, 0, (PetscObject) fe);CHKERRQ(ierr);
  ierr = SetupProblem(prob, user);CHKERRQ(ierr);
  while (cdm) {
    ierr = DMSetDS(cdm,prob);CHKERRQ(ierr);
    ierr = DMGetCoordinateDM(cdm,&coordDM);CHKERRQ(ierr);
    
    /* Auxilliary function */
    ierr = DMClone(cdm, &dmAux);CHKERRQ(ierr);
    ierr = DMSetCoordinateDM(dmAux, coordDM);CHKERRQ(ierr);
    ierr = DMSetDS(dmAux, probAux);CHKERRQ(ierr);
    ierr = PetscObjectCompose((PetscObject) dm, "dmAux", (PetscObject) dmAux);CHKERRQ(ierr);
    ierr = DMCreateLocalVector(dmAux, &f);CHKERRQ(ierr);
    ierr = DMProjectFunctionLocal(dmAux, 0.0, matFuncs, ctx, INSERT_ALL_VALUES, f);CHKERRQ(ierr);
    ierr = PetscObjectCompose((PetscObject) dm, "A", (PetscObject) f);CHKERRQ(ierr);
    ierr = VecDestroy(&f);CHKERRQ(ierr);
    ierr = DMDestroy(&dmAux);CHKERRQ(ierr);
    
    ierr = DMGetCoarseDM(cdm, &cdm);CHKERRQ(ierr);
  }
  
  ierr = PetscFEDestroy(&fe);CHKERRQ(ierr);
  ierr = PetscFEDestroy(&feAux);CHKERRQ(ierr);
  ierr = PetscDSDestroy(&probAux);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

int main(int argc, char **argv)
{
  DM             dm;          /* Problem specification */
  SNES           snes;        /* nonlinear solver */
  Vec            u;           /* solution vector */
  Mat            A,J;         /* Jacobian matrix */
  AppCtx         user;        /* user-defined work context */
  PetscReal      doa,doe,dos,error;
  void           *ctx[1];
  PetscErrorCode ierr;
  PetscInt       dof;
  PetscLogDouble tstart,tend,ttotal;

  ierr = PetscInitialize(&argc, &argv, NULL,help);if (ierr) return ierr;
  ierr = ProcessOptions(PETSC_COMM_WORLD, &user);CHKERRQ(ierr);
  ierr = SNESCreate(PETSC_COMM_WORLD, &snes);CHKERRQ(ierr);
  ierr = CreateMesh(PETSC_COMM_WORLD, &user, &dm);CHKERRQ(ierr);
  ierr = SNESSetDM(snes, dm);CHKERRQ(ierr);
  ierr = DMSetApplicationContext(dm, &user);CHKERRQ(ierr);

  ierr = PetscMalloc(1, &user.exactFuncs);CHKERRQ(ierr);
  ierr = SetupDiscretization(dm, &user);CHKERRQ(ierr);

  ierr = DMCreateGlobalVector(dm, &u);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) u, "potential");CHKERRQ(ierr);

  ierr = DMCreateMatrix(dm, &J);CHKERRQ(ierr);
  A = J;
  
  ierr = DMPlexSetSNESLocalFEM(dm,&user,&user,&user);CHKERRQ(ierr);
  ierr = SNESSetJacobian(snes, A, J, NULL, NULL);CHKERRQ(ierr);

  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
  ctx[0] = &user;
  ierr = DMProjectFunction(dm, 0.0, user.exactFuncs, ctx, INSERT_ALL_VALUES, u);CHKERRQ(ierr);
  
  
  PetscErrorCode (*initialGuess[1])(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nc, PetscScalar u[], void *ctx) = {zero};
  ierr = DMProjectFunction(dm, 0.0, initialGuess, NULL, INSERT_VALUES, u);CHKERRQ(ierr);
  
  /* Measure time */
  ierr = PetscTime(&tstart);CHKERRQ(ierr);
  ierr = SNESSolve(snes, NULL, u);CHKERRQ(ierr);
  ierr = PetscTime(&tend);CHKERRQ(ierr);

  /* Post processing */
  ierr = SNESGetSolution(snes, &u);CHKERRQ(ierr);
  ierr = SNESGetDM(snes, &dm);CHKERRQ(ierr);
  ierr = DMComputeL2Diff(dm, 0.0, user.exactFuncs, ctx, u, &error);CHKERRQ(ierr);
  ttotal = tend-tstart;
  ierr = VecGetSize(u,&dof);CHKERRQ(ierr);
  doa = -PetscLog10Real(error);
  doe = -PetscLog10Real(error*ttotal);
  dos = PetscLog10Real(dof);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "Wall-clock time: %1.3e\n",ttotal);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "L2 error norm: %1.3e\n",error);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "DoF: %d\n",dof);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "DoA: %1.3e\n",doa);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "DoE: %1.3e\n",doe);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "DoS: %1.3e\n",dos);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "Dofsec: %1.3e\n",dof/ttotal);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "True Dofsec: %1.3e\n",doa/dos*dof/ttotal);CHKERRQ(ierr);
  ierr = VecViewFromOptions(u, NULL, "-vec_view");CHKERRQ(ierr);

  /* Clean up */
  if (A != J) {ierr = MatDestroy(&A);CHKERRQ(ierr);}
  ierr = MatDestroy(&J);CHKERRQ(ierr);
  ierr = VecDestroy(&u);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  ierr = PetscFree(user.exactFuncs);CHKERRQ(ierr);
  ierr = PetscFinalize();
  return ierr;
}
