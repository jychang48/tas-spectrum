from firedrake import *
from mpi4py import MPI
import math,sys,time
import numpy as np
eletype = int(sys.argv[1])
FEM = sys.argv[2]
order = int(sys.argv[3])
seed = int(sys.argv[4])
#========;
#  Mesh  ;
#========;
from firedrake.mg.impl import filter_exterior_facet_labels

rank = MPI.COMM_WORLD.Get_rank()
size = MPI.COMM_WORLD.Get_size()
if rank == 0:
  print("Discretization: %s%d on %d MPI processes" % (FEM,order,size))
if eletype == 0:
  mesh = UnitCubeMesh(seed,seed,seed)
else:
  meshbase = UnitSquareMesh(seed,seed,quadrilateral=True)
  mesh = ExtrudedMesh(meshbase,seed)
Q = FunctionSpace(mesh,FEM,order)
Q2 = FunctionSpace(mesh,FEM,order+3)
D0 = FunctionSpace(mesh,'DG',0)
u = TrialFunction(Q)
v = TestFunction(Q)
u_h = Function(Q)
u_bc = Function(Q)
with u_h.dat.vec as u_h_vec:
  dof = u_h_vec.getSize()
if rank == 0:
  print("\tDegrees-of-freedom: %d" % dof)

#==================;
#  Exact solution  ;
#==================;
x = SpatialCoordinate(Q.mesh())
f = interpolate(12*math.pi*math.pi*sin(2*x[0]*math.pi)*sin(2*x[1]*math.pi)*sin(2*x[2]*math.pi),Q)
u_bc = interpolate(sin(2*x[0]*math.pi)*sin(2*x[1]*math.pi)*sin(2*x[2]*math.pi),Q)
u_exact = interpolate(sin(2*x[0]*math.pi)*sin(2*x[1]*math.pi)*sin(2*x[2]*math.pi),Q2)

#=======================;
#  Boundary conditions  ;
#=======================;
if FEM == 'CG':
  if eletype == 0:
    bcs = DirichletBC(Q,Constant(0.0),(1,2,3,4,5,6))
  else:
    bc1 = DirichletBC(Q,Constant(0.0),(1,2,3,4))
    bc2 = DirichletBC(Q,Constant(0.0),"bottom")
    bc3 = DirichletBC(Q,Constant(0.0),"top")
    bcs = [bc1,bc2,bc3]
elif FEM == 'DG':
  bcs = []

#=============;
#  Weak form  ;
#=============;
if FEM == 'CG':
  a = inner(grad(u),grad(v))*dx
  L = f*v*dx
elif FEM == 'DG':
  if eletype == 1:
    dS = dS_h + dS_v
    ds = ds_v + ds_t + ds_b
  Area = FacetArea(mesh)
  Volume = CellVolume(mesh)
  n = FacetNormal(mesh)
  alpha = Constant((float(order)+1)*(float(order)+float(3))/float(3))
  #h = 1/float(seed)
  u0 = u_bc
  Area_avg = (Area('+')+Area('-'))/2
  Volume_avg = (Volume('+')+Volume('+'))/2
  a = inner(grad(v),grad(u))*dx \
    - dot(avg(grad(v)),jump(u,n))*dS \
    - dot(jump(v,n),avg(grad(u)))*dS \
    + Constant(0.5)*alpha*Area_avg/Volume_avg*dot(jump(v,n), jump(u, n))*dS \
    - dot(grad(v), u*n)*ds \
    - dot(v*n, grad(u))*ds \
    + (alpha*Area/Volume)*dot(v, u)*ds
  L = v*f*dx - u0*dot(grad(v), n)*ds + (alpha*Area/Volume)*u0*v*ds

#=====================;
#  Solver parameters  ;
#=====================;
solver_params = {
  'ksp_type': 'cg',
  'pc_type': 'hypre',
  'pc_hypre_boomeramg_strong_threshold': 0.75,
  'pc_hypre_boomeramg_agg_nl': 2,
  'ksp_converged_reason': None,
  'ksp_rtol': '1e-7'
}

#=================;
#  Solve problem  ;
#=================;
initialTime = time.time()
solve(a == L, u_h, bcs, solver_parameters=solver_params)
TotalTime = time.time() - initialTime

#================;
#  Compute info  ;
#================;
L2_error = errornorm(u_exact,u_h,'L2')
dofsec = dof/TotalTime
doa = np.multiply(np.log10(L2_error),-1.0)
doasec = doa/TotalTime
if rank == 0:
  print("\tWall-clock time:  %1.3e seconds" % TotalTime)
  print("\tL2 error norm: %1.3e" % L2_error)
  print("\tDigits of accuracy:  %1.3e" % doa)
  print("\tDoA/s:  %1.3e" % doasec)
  print("\tDoF/s:  %1.3e" % dofsec)

#outfile = File("poisson.pvd")
#outfile.write(u_bc,u_h)
