/* ---------------------------------------------------------------------
 *
 * Copyright (C) 2010 - 2017 by the deal.II authors
 *
 * This file is part of the deal.II library.
 *
 * The deal.II library is free software; you can use it, redistribute
 * it, and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * The full text of the license can be found in the file LICENSE at
 * the top level of the deal.II distribution.
 *
 * ---------------------------------------------------------------------

 *
 * Author: Maurice S. Fabien, Rice University, 2017
 */


// The include files for the linear algebra: A regular SparseMatrix, which in
// turn will include the necessary files for SparsityPattern and Vector
// classes.
#include <deal.II/lac/sparse_matrix.h>             // Not needed as PETSc has its own matrix/vector containers
#include <deal.II/lac/dynamic_sparsity_pattern.h>  // Not needed as PETSc has its own matrix/vector containers
#include <deal.II/lac/solver_cg.h>                 // Not needed as PETSc has its own solvers
#include <deal.II/lac/precondition.h>              // Not needed as PETSc has its own preconditioners
#include <deal.II/lac/precondition_block.h>        // Not needed as PETSc has its own preconditioners
#include <deal.II/lac/block_vector.h>              

// Include files for setting up the mesh
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/grid_refinement.h>

// Include files for FiniteElement classes and DoFHandler.
#include <deal.II/fe/fe_q.h>
#include <deal.II/fe/fe_dgp.h>
#include <deal.II/fe/fe_dgq.h>
#include <deal.II/dofs/dof_tools.h>

// The include files for using the MeshWorker framework
#include <deal.II/meshworker/dof_info.h>
#include <deal.II/meshworker/integration_info.h>
#include <deal.II/meshworker/assembler.h>
#include <deal.II/meshworker/loop.h>

// The include file for local integrators associated with the Laplacian
#include <deal.II/integrators/laplace.h>

#include <deal.II/lac/petsc_sparse_matrix.h>   // Includes PETSc matrix containers
#include <deal.II/lac/petsc_vector.h>          // Includes PETSc vector containers

#include <deal.II/lac/petsc_solver.h>          // Includes PETSc solvers
#include <deal.II/lac/petsc_precondition.h>    // Includes PETSc preconditioner

// Support for geometric multigrid methods
/*
#include <deal.II/multigrid/mg_tools.h>
#include <deal.II/multigrid/multigrid.h>
#include <deal.II/multigrid/mg_matrix.h>
#include <deal.II/multigrid/mg_transfer.h>
#include <deal.II/multigrid/mg_coarse.h>
#include <deal.II/multigrid/mg_smoother.h>
*/

// Finally, we take our exact solution from the library as well as quadrature
// and additional tools.
#include <deal.II/base/function_lib.h>      //Not needed, we provide a manufactured solution
#include <deal.II/base/quadrature_lib.h>
#include <deal.II/numerics/vector_tools.h>
#include <deal.II/numerics/data_out.h>

//CPU/wall clock
#include <deal.II/base/timer.h>

#include <iostream>
#include <fstream>

// All classes of the deal.II library are in the namespace dealii. In order to
// save typing, we tell the compiler to search names in there as well.
namespace Step39
{
  using namespace dealii;
  
	template <int dim>
	class Solution : public Function<dim>
	{
	public:
	  Solution () : Function<dim>() {}
	
	  virtual double value (const Point<dim>   &p,
	                        const unsigned int  component = 0) const;
	};  
  
	template <int dim>
	double Solution<dim>::value (const Point<dim>   &p,
	                               const unsigned int) const
	{
	  double return_value = 0.0;
	  if (dim == 1){
	    return_value = std::sin( 2.0 * numbers::PI * p(0) );
	  }
	  else if (dim == 2){
	    //return_value =  p(0) + p(1) ; //laplace
	    //return_value = std::sin( 2.0 * numbers::PI * p(0) ) * std::sin( 2.0 * numbers::PI * p(1) );
	double x2 = p(0) * p(0);
	double y2 = p(1) * p(1);	  
    return_value = std::sin( 2.0 * numbers::PI * x2 ) * std::sin( 2.0 * numbers::PI * y2 );
	//4 π (sin(2 π x^2) cos(2 π y^2) + sin(2 π y^2) (cos(2 π x^2) - 4 π (x^2 + y^2) sin(2 π x^2))) 	    
	  }
	  else if (dim == 3){
	    return_value = std::sin( 2.0 * numbers::PI * p(0) ) * std::sin( 2.0 * numbers::PI * p(1) )* std::sin( 2.0 * numbers::PI * p(2) );
	  }
	
	  return return_value;
	}


	template <int dim>
	class RightHandSide : public Function<dim>
	{
	public:
	  RightHandSide () : Function<dim>() {}
	
	  virtual double value (const Point<dim>   &p,
	                        const unsigned int  component = 0) const;
	};
	
	template <int dim>
	double RightHandSide<dim>::value (const Point<dim> &p,
	                                  const unsigned int /*component*/) const
	{
	  double return_value = 0.0;
	  double pi_squared   = (numbers::PI)*(numbers::PI);
	  if (dim == 1){
	    return_value = 4.0 * pi_squared * std::sin( 2.0 * numbers::PI * p(0) );
	  }
	  else if (dim == 2){
	    //return_value = 8.0 * pi_squared * std::sin( 2.0 * numbers::PI * p(0) ) * std::sin( 2.0 * numbers::PI * p(1) );
	double x2 = p(0) * p(0);
	double y2 = p(1) * p(1);		  
    return_value = -4.0 * (numbers::PI)* (std::sin(2 * (numbers::PI)* x2) * std::cos(2 * (numbers::PI)* y2) + std::sin(2 * (numbers::PI)* y2) * (std::cos(2 * (numbers::PI)*x2) - 4 * (numbers::PI)* (x2 + y2) * std::sin(2 * (numbers::PI)* x2) ));    	  
    //return_value = 8.0 * pi_squared * std::sin( 2.0 * numbers::PI * p(0) ) * std::sin( 2.0 * numbers::PI * p(1) );	    
	  }
	  else if (dim == 3){
	    return_value = 12.0 * pi_squared * std::sin( 2.0 * numbers::PI * p(0) ) * std::sin( 2.0 * numbers::PI * p(1) )* std::sin( 2.0 * numbers::PI * p(2) );
	  }
	  
	  return return_value;
	}

  // @sect3{The local integrators}

  // MeshWorker separates local integration from the loops over cells and
  // faces. Thus, we have to write local integration classes for generating
  // matrices, the right hand side and the error estimator.

  // All these classes have the same three functions for integrating over
  // cells, boundary faces and interior faces, respectively. All the
  // information needed for the local integration is provided by
  // MeshWorker::IntegrationInfo<dim>. Note that the signature of the
  // functions cannot be changed, because it is expected by
  // MeshWorker::integration_loop().

  // The first class defining local integrators is responsible for computing
  // cell and face matrices. It is used to assemble the global matrix as well
  // as the level matrices.
  template <int dim>
  class MatrixIntegrator : public MeshWorker::LocalIntegrator<dim>
  {
  public:
    void cell(MeshWorker::DoFInfo<dim> &dinfo,
              typename MeshWorker::IntegrationInfo<dim> &info) const;
    void boundary(MeshWorker::DoFInfo<dim> &dinfo,
                  typename MeshWorker::IntegrationInfo<dim> &info) const;
    void face(MeshWorker::DoFInfo<dim> &dinfo1,
              MeshWorker::DoFInfo<dim> &dinfo2,
              typename MeshWorker::IntegrationInfo<dim> &info1,
              typename MeshWorker::IntegrationInfo<dim> &info2) const;
  };


  // On each cell, we integrate the Dirichlet form. We use the library of
  // ready made integrals in LocalIntegrators to avoid writing these loops
  // ourselves. Similarly, we implement Nitsche boundary conditions and the
  // interior penalty fluxes between cells.
  //
  // The boundary and flux terms need a penalty parameter, which should be
  // adjusted to the cell size and the polynomial degree. A safe choice of
  // this parameter for constant coefficients can be found in
  // LocalIntegrators::Laplace::compute_penalty() and we use this below.
  template <int dim>
  void MatrixIntegrator<dim>::cell(
    MeshWorker::DoFInfo<dim> &dinfo,
    typename MeshWorker::IntegrationInfo<dim> &info) const
  {
    LocalIntegrators::Laplace::cell_matrix(dinfo.matrix(0,false).matrix, info.fe_values());
  }


  template <int dim>
  void MatrixIntegrator<dim>::boundary(
    MeshWorker::DoFInfo<dim> &dinfo,
    typename MeshWorker::IntegrationInfo<dim> &info) const
  {
    const unsigned int deg = info.fe_values(0).get_fe().tensor_degree();
    LocalIntegrators::Laplace::nitsche_matrix(
      dinfo.matrix(0,false).matrix, info.fe_values(0),
      LocalIntegrators::Laplace::compute_penalty(dinfo, dinfo, deg, deg));
  }

  // Interior faces use the interior penalty method
  template <int dim>
  void MatrixIntegrator<dim>::face(
    MeshWorker::DoFInfo<dim> &dinfo1,
    MeshWorker::DoFInfo<dim> &dinfo2,
    typename MeshWorker::IntegrationInfo<dim> &info1,
    typename MeshWorker::IntegrationInfo<dim> &info2) const
  {
    const unsigned int deg = info1.fe_values(0).get_fe().tensor_degree();
    LocalIntegrators::Laplace::ip_matrix(
      dinfo1.matrix(0,false).matrix, dinfo1.matrix(0,true).matrix,
      dinfo2.matrix(0,true).matrix, dinfo2.matrix(0,false).matrix,
      info1.fe_values(0), info2.fe_values(0),
      LocalIntegrators::Laplace::compute_penalty(dinfo1, dinfo2, deg, deg));
  }

  // The second local integrator builds the right hand side. In our example,
  // the right hand side function is zero, such that only the boundary
  // condition is set here in weak form.
  template <int dim>
  class RHSIntegrator : public MeshWorker::LocalIntegrator<dim>
  {
  public:
    void cell(MeshWorker::DoFInfo<dim> &dinfo, typename MeshWorker::IntegrationInfo<dim> &info) const;
    void boundary(MeshWorker::DoFInfo<dim> &dinfo, typename MeshWorker::IntegrationInfo<dim> &info) const;
    void face(MeshWorker::DoFInfo<dim> &dinfo1,
              MeshWorker::DoFInfo<dim> &dinfo2,
              typename MeshWorker::IntegrationInfo<dim> &info1,
              typename MeshWorker::IntegrationInfo<dim> &info2) const;
  };


  //cell rhs, notice minus sign is corrected in right_hand_side later
  template <int dim>
  void RHSIntegrator<dim>::cell(MeshWorker::DoFInfo<dim> &dinfo, typename MeshWorker::IntegrationInfo<dim> &info) const
  {
	  const FEValuesBase<dim> &fe = info.fe_values();
	  const RightHandSide<dim> right_hand_side;
	  
	  Vector<double> &local_vector = dinfo.vector(0).block(0);
	  

    for (unsigned k=0; k<fe.n_quadrature_points; ++k){
      for (unsigned int i=0; i<fe.dofs_per_cell; ++i){		 
	  		local_vector(i) += -(fe.shape_value (i, k) *
                            right_hand_side.value (fe.quadrature_point (k)) *
                            fe.JxW (k));
					   }//end i
				   }//end k	  
 
	  
  }


  template <int dim>
  void RHSIntegrator<dim>::boundary(MeshWorker::DoFInfo<dim> &dinfo, typename MeshWorker::IntegrationInfo<dim> &info) const
  {
    const FEValuesBase<dim> &fe = info.fe_values();
    Vector<double> &local_vector = dinfo.vector(0).block(0);

    std::vector<double> boundary_values(fe.n_quadrature_points);
    
    const Solution<dim> exact_solution;
    
    const RightHandSide<dim> right_hand_side;
    
    
    for (unsigned k=0; k<fe.n_quadrature_points; ++k){
		boundary_values[k] = exact_solution.value(fe.quadrature_point (k));
		//exact_solution.value (fe.quadrature_point (q_index));
	}
    //exact_solution.value_list(fe.get_quadrature_points(), boundary_values);
	 

    const unsigned int deg = fe.get_fe().tensor_degree();
    //const double penalty = 2. * (deg+1) * (deg+dim) / (dim * dinfo.face->measure());
    //Shahbazi
    const double penalty = (deg+1) * (deg+dim) *(dinfo.face->measure()) / (dim * dinfo.cell->measure());


    for (unsigned k=0; k<fe.n_quadrature_points; ++k){
      for (unsigned int i=0; i<fe.dofs_per_cell; ++i){		 
        local_vector(i) += (- fe.shape_value(i,k) * penalty * boundary_values[k]
                            + (fe.normal_vector(k) * fe.shape_grad(i,k)) * boundary_values[k])
                           * fe.JxW(k);
					   }//end i
				   }//end k
  }


  template <int dim>
  void RHSIntegrator<dim>::face(MeshWorker::DoFInfo<dim> &,
                                MeshWorker::DoFInfo<dim> &,
                                typename MeshWorker::IntegrationInfo<dim> &,
                                typename MeshWorker::IntegrationInfo<dim> &) const
  {}


  // The third local integrator is responsible for the contributions to the
  // error estimate. This is the standard energy estimator due to Karakashian
  // and Pascal (2003).
  template <int dim>
  class Estimator : public MeshWorker::LocalIntegrator<dim>
  {
  public:
    void cell(MeshWorker::DoFInfo<dim> &dinfo, typename MeshWorker::IntegrationInfo<dim> &info) const;
    void boundary(MeshWorker::DoFInfo<dim> &dinfo, typename MeshWorker::IntegrationInfo<dim> &info) const;
    void face(MeshWorker::DoFInfo<dim> &dinfo1,
              MeshWorker::DoFInfo<dim> &dinfo2,
              typename MeshWorker::IntegrationInfo<dim> &info1,
              typename MeshWorker::IntegrationInfo<dim> &info2) const;
  };


  // The cell contribution is the Laplacian of the discrete solution, since
  // the right hand side is zero.
  template <int dim>
  void Estimator<dim>::cell(MeshWorker::DoFInfo<dim> &dinfo, typename MeshWorker::IntegrationInfo<dim> &info) const
  {
    const FEValuesBase<dim> &fe = info.fe_values();

    const std::vector<Tensor<2,dim> > &DDuh = info.hessians[0][0];
    for (unsigned k=0; k<fe.n_quadrature_points; ++k)
      {
        const double t = dinfo.cell->diameter() * trace(DDuh[k]);
        dinfo.value(0) +=  t*t * fe.JxW(k);
      }
    dinfo.value(0) = std::sqrt(dinfo.value(0));
  }

  // At the boundary, we use simply a weighted form of the boundary residual,
  // namely the norm of the difference between the finite element solution and
  // the correct boundary condition.
  template <int dim>
  void Estimator<dim>::boundary(MeshWorker::DoFInfo<dim> &dinfo, typename MeshWorker::IntegrationInfo<dim> &info) const
  {
    const FEValuesBase<dim> &fe = info.fe_values();

    std::vector<double> boundary_values(fe.n_quadrature_points);
    
    const Solution<dim> exact_solution;

    const std::vector<double> &uh = info.values[0][0];

    const unsigned int deg = fe.get_fe().tensor_degree();
    //const double penalty = 2. * (deg+1) * (deg+dim) * 1 / (dim * dinfo.face->measure());

    const double penalty = (deg+1) * (deg+dim) *(dinfo.face->measure()) / (dim * dinfo.cell->measure());

    for (unsigned k=0; k<fe.n_quadrature_points; ++k)
      dinfo.value(0) += penalty * (boundary_values[k] - uh[k]) * (boundary_values[k] - uh[k])
                        * fe.JxW(k);
    dinfo.value(0) = std::sqrt(dinfo.value(0));
  }


  // Finally, on interior faces, the estimator consists of the jumps of the
  // solution and its normal derivative, weighted appropriately.
  template <int dim>
  void Estimator<dim>::face(MeshWorker::DoFInfo<dim> &dinfo1,
                            MeshWorker::DoFInfo<dim> &dinfo2,
                            typename MeshWorker::IntegrationInfo<dim> &info1,
                            typename MeshWorker::IntegrationInfo<dim> &info2) const
  {
    const FEValuesBase<dim> &fe = info1.fe_values();
    const std::vector<double> &uh1 = info1.values[0][0];
    const std::vector<double> &uh2 = info2.values[0][0];
    const std::vector<Tensor<1,dim> > &Duh1 = info1.gradients[0][0];
    const std::vector<Tensor<1,dim> > &Duh2 = info2.gradients[0][0];

    const unsigned int deg = fe.get_fe().tensor_degree();
    const double penalty1 = (deg+1) * (deg+dim) * dinfo1.face->measure() / (dim * dinfo1.cell->measure());
    const double penalty2 = (deg+1) * (deg+dim) * dinfo2.face->measure() / (dim * dinfo2.cell->measure());
//    const double penalty1 = 2. * (deg+1) * (deg+dim) * 1 / (dim * dinfo1.face->measure());
//    const double penalty2 = 2. * (deg+1) * (deg+dim) * 1 / (dim * dinfo2.face->measure());
    
    
    const double penalty = penalty1 + penalty2;
    const double h = dinfo1.face->measure();

    for (unsigned k=0; k<fe.n_quadrature_points; ++k)
      {
        double diff1 = uh1[k] - uh2[k];
        double diff2 = fe.normal_vector(k) * Duh1[k] - fe.normal_vector(k) * Duh2[k];
        dinfo1.value(0) += (penalty * diff1*diff1 + h * diff2*diff2)
                           * fe.JxW(k);
      }
    dinfo1.value(0) = std::sqrt(dinfo1.value(0));
    dinfo2.value(0) = dinfo1.value(0);
  }

  // Finally we have an integrator for the error. Since the energy norm for
  // discontinuous Galerkin problems not only involves the difference of the
  // gradient inside the cells, but also the jump terms across faces and at
  // the boundary, we cannot just use VectorTools::integrate_difference().
  // Instead, we use the MeshWorker interface to compute the error ourselves.

  // There are several different ways to define this energy norm, but all of
  // them are equivalent to each other uniformly with mesh size (some not
  // uniformly with polynomial degree). Here, we choose @f[ \|u\|_{1,h} =
  // \sum_{K\in \mathbb T_h} \|\nabla u\|_K^2 + \sum_{F \in F_h^i}
  // 4\sigma_F\|\{\!\{ u \mathbf n\}\!\}\|^2_F + \sum_{F \in F_h^b}
  // 2\sigma_F\|u\|^2_F @f]

  template <int dim>
  class ErrorIntegrator : public MeshWorker::LocalIntegrator<dim>
  {
  public:
    void cell(MeshWorker::DoFInfo<dim> &dinfo, typename MeshWorker::IntegrationInfo<dim> &info) const;
    void boundary(MeshWorker::DoFInfo<dim> &dinfo, typename MeshWorker::IntegrationInfo<dim> &info) const;
    void face(MeshWorker::DoFInfo<dim> &dinfo1,
              MeshWorker::DoFInfo<dim> &dinfo2,
              typename MeshWorker::IntegrationInfo<dim> &info1,
              typename MeshWorker::IntegrationInfo<dim> &info2) const;
  };

  // Here we have the integration on cells. There is currently no good
  // interface in MeshWorker that would allow us to access values of regular
  // functions in the quadrature points. Thus, we have to create the vectors
  // for the exact function's values and gradients inside the cell
  // integrator. After that, everything is as before and we just add up the
  // squares of the differences.

  // Additionally to computing the error in the energy norm, we use the
  // capability of the mesh worker to compute two functionals at the same time
  // and compute the <i>L<sup>2</sup></i>-error in the same loop. Obviously,
  // this one does not have any jump terms and only appears in the integration
  // on cells.
  template <int dim>
  void ErrorIntegrator<dim>::cell(
    MeshWorker::DoFInfo<dim> &dinfo,
    typename MeshWorker::IntegrationInfo<dim> &info) const
  {
	  
    const FEValuesBase<dim> &fe = info.fe_values();
    std::vector<Tensor<1,dim> > exact_gradients(fe.n_quadrature_points);
    std::vector<double> exact_values(fe.n_quadrature_points);

    const Solution<dim> exact_solution;

	for (unsigned int q_index=0; q_index<fe.n_quadrature_points; ++q_index){
		exact_values[q_index] = exact_solution.value (fe.quadrature_point (q_index));
	}

    const std::vector<Tensor<1,dim> > &Duh = info.gradients[0][0];
    const std::vector<double> &uh = info.values[0][0];

    for (unsigned k=0; k<fe.n_quadrature_points; ++k)
      {
        double sum = 0;
        for (unsigned int d=0; d<dim; ++d)
          {
            const double diff = exact_gradients[k][d] - Duh[k][d];
            sum += diff*diff;
          }
        const double diff = exact_values[k] - uh[k];
        dinfo.value(0) +=  sum * fe.JxW(k);
        dinfo.value(1) +=  diff*diff * fe.JxW(k);
      }
    dinfo.value(0) = std::sqrt(dinfo.value(0));
    dinfo.value(1) = std::sqrt(dinfo.value(1));
  }

  //has an issue here
  template <int dim>
  void ErrorIntegrator<dim>::boundary(
    MeshWorker::DoFInfo<dim> &dinfo,
    typename MeshWorker::IntegrationInfo<dim> &info) const
  {
    const FEValuesBase<dim> &fe = info.fe_values();

    std::vector<double> exact_values(fe.n_quadrature_points);
    
    const Solution<dim> exact_solution;    
    
    QGauss<dim>  quadrature_formula( 5 ); //p=1,q=2  

		for (unsigned int q_index=0; q_index<fe.n_quadrature_points; ++q_index){
			exact_values[q_index] = exact_solution.value (fe.quadrature_point (q_index));
		}    

    const std::vector<double> &uh = info.values[0][0];

    const unsigned int deg = fe.get_fe().tensor_degree();
    //const double penalty = 2. * (deg+1) * (deg+dim) * 1 / (dim * dinfo.face->measure());
    const double penalty = (deg+1) * (deg+dim) *(dinfo.face->measure()) / (dim * dinfo.cell->measure());

    for (unsigned k=0; k<fe.n_quadrature_points; ++k)
      {
        const double diff = exact_values[k] - uh[k];
        dinfo.value(0) += penalty * diff * diff * fe.JxW(k);
      }
    dinfo.value(0) = std::sqrt(dinfo.value(0));
  }


  template <int dim>
  void ErrorIntegrator<dim>::face(
    MeshWorker::DoFInfo<dim> &dinfo1,
    MeshWorker::DoFInfo<dim> &dinfo2,
    typename MeshWorker::IntegrationInfo<dim> &info1,
    typename MeshWorker::IntegrationInfo<dim> &info2) const
  {
    const FEValuesBase<dim> &fe = info1.fe_values();
    const std::vector<double> &uh1 = info1.values[0][0];
    const std::vector<double> &uh2 = info2.values[0][0];

    const unsigned int deg = fe.get_fe().tensor_degree();
    
    //const double penalty1 = 2. * (deg+1) * (deg+dim) * 1 / (dim * dinfo1.face->measure());
    //const double penalty2 = 2. * (deg+1) * (deg+dim) * 1 / (dim * dinfo2.face->measure());
    const double penalty1 = (deg+1) * (deg+dim) *(dinfo1.face->measure()) / (dim * dinfo1.cell->measure());
    const double penalty2 = (deg+1) * (deg+dim) *(dinfo2.face->measure()) / (dim * dinfo2.cell->measure());

    const double penalty = penalty1 + penalty2;

    for (unsigned k=0; k<fe.n_quadrature_points; ++k)
      {
        double diff = uh1[k] - uh2[k];
        dinfo1.value(0) += (penalty * diff*diff)
                           * fe.JxW(k);
      }
    dinfo1.value(0) = std::sqrt(dinfo1.value(0));
    dinfo2.value(0) = dinfo1.value(0);
  }



  // @sect3{The main class}

  // This class does the main job, like in previous examples. For a
  // description of the functions declared here, please refer to the
  // implementation below.
  template <int dim>
  class InteriorPenaltyProblem
  {
  public:
    typedef MeshWorker::IntegrationInfo<dim> CellInfo;

    InteriorPenaltyProblem(const FiniteElement<dim> &fe);

    void run(unsigned int n_steps);

  private:
    void setup_system ();
    void assemble_matrix ();
    void assemble_right_hand_side ();
    void error ();
    double estimate ();
    void solve ();
    void output_results ( ) const;

    // The member objects related to the discretization are here.
    Triangulation<dim>        triangulation;
    const MappingQ1<dim>      mapping;
    const FiniteElement<dim> &fe;
    DoFHandler<dim>           dof_handler;

    // Then, we have the matrices and vectors related to the global discrete
    // system.
    SparsityPattern      sparsity;     
    
	PETScWrappers::SparseMatrix matrix;
	PETScWrappers::Vector       solution;
	PETScWrappers::Vector       right_hand_side;  
	BlockVector<double>  estimates;

  };


  // The constructor simply sets up the coarse grid and the DoFHandler. The
  // FiniteElement is provided as a parameter to allow flexibility.
  template <int dim>
  InteriorPenaltyProblem<dim>::InteriorPenaltyProblem(const FiniteElement<dim> &fe)
    :
    mapping(),
    fe(fe),
    dof_handler(triangulation),
    estimates(1)
  {
    //GridGenerator::hyper_cube(triangulation, -1, 1);
      const unsigned int  	repetitions = 10;  //forces a uniform mesh [0,1]^dim with repetitions^dim cells
      GridGenerator::subdivided_hyper_cube 	(triangulation,
	  	                                 repetitions,
	  	                                 0.0,
	  	                                 1.0);    
  }


  // In this function, we set up the dimension of the linear system and the
  // sparsity patterns for the global matrix as well as the level matrices.
  template <int dim>
  void
  InteriorPenaltyProblem<dim>::setup_system()
  {
    // First, we use the finite element to distribute degrees of freedom over
    // the mesh and number them.
    dof_handler.distribute_dofs(fe);
    //dof_handler.distribute_mg_dofs(fe);
    unsigned int n_dofs = dof_handler.n_dofs();
    // Then, we already know the size of the vectors representing finite
    // element functions.
    solution.reinit(n_dofs);
    right_hand_side.reinit(n_dofs);

    // Next, we set up the sparsity pattern for the global matrix. Since we do
    // not know the row sizes in advance, we first fill a temporary
    // DynamicSparsityPattern object and copy it to the regular
    // SparsityPattern once it is complete.
    DynamicSparsityPattern dsp(n_dofs);
    DoFTools::make_flux_sparsity_pattern(dof_handler, dsp);
    sparsity.copy_from(dsp);
    matrix.reinit(sparsity);
  }


  // In this function, we assemble the global system matrix, where by global
  // we indicate that this is the matrix of the discrete system we solve and
  // it is covering the whole mesh.
  template <int dim>
  void
  InteriorPenaltyProblem<dim>::assemble_matrix()
  {
    // First, we need t set up the object providing the values we
    // integrate. This object contains all FEValues and FEFaceValues objects
    // needed and also maintains them automatically such that they always
    // point to the current cell. To this end, we need to tell it first, where
    // and what to compute. Since we are not doing anything fancy, we can rely
    // on their standard choice for quadrature rules.
    //
    // Since their default update flags are minimal, we add what we need
    // additionally, namely the values and gradients of shape functions on all
    // objects (cells, boundary and interior faces). Afterwards, we are ready
    // to initialize the container, which will create all necessary
    // FEValuesBase objects for integration.
    MeshWorker::IntegrationInfoBox<dim> info_box;
    UpdateFlags update_flags = update_values | update_gradients;
    info_box.add_update_flags_all(update_flags);
    info_box.initialize(fe, mapping);

    // This is the object into which we integrate local data. It is filled by
    // the local integration routines in MatrixIntegrator and then used by the
    // assembler to distribute the information into the global matrix.
    MeshWorker::DoFInfo<dim> dof_info(dof_handler);

    // Furthermore, we need an object that assembles the local matrix into the
    // global matrix. These assembler objects have all the knowledge
    // of the structures of the target object, in this case a
    // SparseMatrix, possible constraints and the mesh structure.
    //MeshWorker::Assembler::MatrixSimple<SparseMatrix<double> > assembler;
    //assembler.initialize(matrix);        
                      
    MeshWorker::Assembler::MatrixSimple<PETScWrappers::SparseMatrix > assembler;
   
	assembler.initialize(matrix);
	
	/*
	matrix.reinit(dof_handler.n_dofs(),
                      dof_handler.n_dofs(),
                      dof_handler.max_couplings_between_dofs() );	
	
	solution.reinit ( dof_handler.n_dofs() );
	right_hand_side.reinit ( dof_handler.n_dofs() );
    */

    // Now comes the part we coded ourselves, the local
    // integrator. This is the only part which is problem dependent.
    MatrixIntegrator<dim> integrator;
    // Now, we throw everything into a MeshWorker::loop(), which here
    // traverses all active cells of the mesh, computes cell and face matrices
    // and assembles them into the global matrix. We use the variable
    // <tt>dof_handler</tt> here in order to use the global numbering of
    // degrees of freedom.
    
    MeshWorker::integration_loop<dim, dim>(
      dof_handler.begin_active(), dof_handler.end(),
      dof_info, info_box,
      integrator, assembler);
      
      
	matrix.compress(VectorOperation::add);  //PETSc solvers complain if this is not done
	//right_hand_side.compress(VectorOperation::add);      
		
      
  }

  // Here we have another clone of the assemble function. The difference to
  // assembling the system matrix consists in that we assemble a vector here.
  template <int dim>
  void
  InteriorPenaltyProblem<dim>::assemble_right_hand_side()
  {
    MeshWorker::IntegrationInfoBox<dim> info_box;
    UpdateFlags update_flags = update_quadrature_points | update_values | update_gradients;
    info_box.add_update_flags_all(update_flags);
    info_box.initialize(fe, mapping);

    MeshWorker::DoFInfo<dim> dof_info(dof_handler);

    // Since this assembler allows us to fill several vectors, the interface is
    // a little more complicated as above. The pointers to the vectors have to
    // be stored in a AnyData object. While this seems to cause two extra
    // lines of code here, it actually comes handy in more complex
    // applications.
    MeshWorker::Assembler::ResidualSimple<PETScWrappers::Vector > assembler;
    AnyData data;
    data.add<PETScWrappers::Vector*>(&right_hand_side, "RHS");
    assembler.initialize(data);

    RHSIntegrator<dim> integrator;
    MeshWorker::integration_loop<dim, dim>(
      dof_handler.begin_active(), dof_handler.end(),
      dof_info, info_box,
      integrator, assembler);

    right_hand_side *= -1.;
  }


  // Now that we have coded all functions building the discrete linear system,
  // it is about time that we actually solve it.
  template <int dim>
  void
  InteriorPenaltyProblem<dim>::solve()
  {
    // The solver of choice is PETSc KSP conjugate gradient.
    // The preconditioner is BoomerAMG
    
    SolverControl           solver_control (solution.size(), 1e-7*right_hand_side.l2_norm());
    PETScWrappers::SolverCG cg (solver_control);

    //PETScWrappers::PreconditionBlockJacobi preconditioner(system_matrix);

    PETScWrappers::PreconditionBoomerAMG preconditioner;
    PETScWrappers::PreconditionBoomerAMG::AdditionalData data;
    preconditioner.initialize(matrix, data);

    cg.solve (matrix, solution, right_hand_side,
    	      preconditioner);    



	// After the KSP solve, compute the L2 norm error
	// notice that the quadrature rule QGauss needs to be sufficiently
	// large
	Vector<double> difference_per_cell (triangulation.n_active_cells());
	VectorTools::integrate_difference (dof_handler,
                                   solution,
                                   Solution<dim>(),
                                   difference_per_cell,
                                   QGauss<dim>(4),
                                   VectorTools::L2_norm);
	const double L2_error = difference_per_cell.l2_norm();
	
	//TAS data
	double DoA = -1.0*(std::log10(L2_error));
	double DoS = std::log10(dof_handler.n_dofs());

	std::cout << "   " << "L2-Norm error = " << L2_error << std::endl; 
	std::cout << "   " << "DoS           = " << DoS      << std::endl;

	std::cout << "   " << "DoA           = " << DoA      << std::endl;
	std::cout << "   " << "DoA/DoS       = " << DoA/DoS  << std::endl;
	
	
	//quick save for DoA, DoA/DoS
	std::ofstream outfile1, outfile2, outfile3;
  
	outfile1.open("dedg1_doa", std::ios_base::app);
	outfile1 << std::setprecision(16) << DoA << std::endl;   
  
	outfile2.open("dedg1_dos", std::ios_base::app);
	outfile2 << std::setprecision(16) << DoS << std::endl;   
  
	outfile3.open("dedg1_doados", std::ios_base::app);
	outfile3 << std::setprecision(16) << DoA/DoS << std::endl; 	
	
	
  }


  // Another clone of the assemble function. The big difference to the
  // previous ones is here that we also have an input vector.
  template <int dim>
  double
  InteriorPenaltyProblem<dim>::estimate()
  {  return 0.0; }

  // Here we compare our finite element solution with the (known) exact
  // solution and compute the mean quadratic error of the gradient and the
  // function itself. This function is a clone of the estimation function
  // right above.

  // Since we compute the error in the energy and the
  // <i>L<sup>2</sup></i>-norm, respectively, our block vector needs two
  // blocks here.
  template <int dim>
  void
  InteriorPenaltyProblem<dim>::error()
  {
	  
    BlockVector<double> errors(2);
    errors.block(0).reinit(triangulation.n_active_cells());
    errors.block(1).reinit(triangulation.n_active_cells());
    unsigned int i=0;
    for (typename Triangulation<dim>::active_cell_iterator cell = triangulation.begin_active();
         cell != triangulation.end(); ++cell,++i)
      cell->set_user_index(i);

    MeshWorker::IntegrationInfoBox<dim> info_box;
    const unsigned int n_gauss_points = dof_handler.get_fe().tensor_degree()+1;
    info_box.initialize_gauss_quadrature(n_gauss_points, n_gauss_points+1, n_gauss_points);

    AnyData solution_data;
    solution_data.add<PETScWrappers::Vector *>(&solution, "solution");

    info_box.cell_selector.add("solution", true, true, false);
    info_box.boundary_selector.add("solution", true, false, false);
    info_box.face_selector.add("solution", true, false, false);

    info_box.add_update_flags_cell(update_quadrature_points);
    info_box.add_update_flags_boundary(update_quadrature_points);
    info_box.initialize(fe, mapping, solution_data, solution);

    MeshWorker::DoFInfo<dim> dof_info(dof_handler);

    MeshWorker::Assembler::CellsAndFaces<double> assembler;
    AnyData out_data;
    out_data.add<BlockVector<double>* >(&errors, "cells");
    assembler.initialize(out_data, false);

    ErrorIntegrator<dim> integrator;
    MeshWorker::integration_loop<dim, dim> (
      dof_handler.begin_active(), dof_handler.end(),
      dof_info, info_box,
      integrator, assembler);

    deallog << "energy-error: " << errors.block(0).l2_norm() << std::endl;
    deallog << "L2-error:     " << errors.block(1).l2_norm() << std::endl;
    
  }


  // Some graphical output
  template <int dim>
  void InteriorPenaltyProblem<dim>::output_results ( ) const
  {
    // Output of the solution in gnuplot format.
    /*
    char *fn = new char[100];
    sprintf(fn, "sol-%02d", cycle);

    std::string filename(fn);
    filename += ".gnuplot";
    deallog << "Writing solution to <" << filename << ">..."
            << std::endl << std::endl;
    std::ofstream gnuplot_output (filename.c_str());

    DataOut<dim> data_out;
    data_out.attach_dof_handler (dof_handler);
    data_out.add_data_vector (solution, "u");
    data_out.add_data_vector (estimates.block(0), "est");

    data_out.build_patches ();

    data_out.write_gnuplot(gnuplot_output);
    */
  }

  // And finally the adaptive loop, more or less like in previous examples.
  template <int dim>
  void
  InteriorPenaltyProblem<dim>::run(unsigned int n_steps)
  {
	  
	Timer timer_assemble;
	Timer timer_solve;	  
	
    deallog << "Element: " << fe.get_name() << std::endl;
    for (unsigned int s=0; s<n_steps; ++s)
      {
		  
		if (s > 0 ){
			triangulation.refine_global(1);
		}
		
        deallog << "Triangulation "
                << triangulation.n_active_cells() << " cells, "
                << triangulation.n_levels() << " levels" << std::endl;

        setup_system();

        //deallog << ' ' << dof_handler.n_dofs;
        //deallog << std::endl;

        deallog << "Assemble matrix" << std::endl;
        
		timer_assemble.start ();          
        assemble_matrix();       
		timer_assemble.stop ();        
        
        //deallog << "Assemble multilevel matrix" << std::endl;
        //assemble_mg_matrix();
        deallog << "Assemble right hand side" << std::endl;
        assemble_right_hand_side();
        deallog << "Solve" << std::endl;
        
        
	    timer_solve.start();
	    solve ();
	    timer_solve.stop();
        
	        
		std::cout << "   Elapsed CPU time (assemble): " << timer_assemble() << " seconds."<< std::endl;
		std::cout << "   Elapsed wall time (assemble): " << timer_assemble.wall_time() << " seconds."<< std::endl;
		
		std::cout << "   Elapsed CPU time (solve): " << timer_solve() << " seconds."<< std::endl;
		std::cout << "   Elapsed wall time (solve): " << timer_solve.wall_time() << " seconds."<< std::endl;	          
	    
		std::cout << "   Total elapsed CPU time (assemble+solve): " << timer_assemble()+timer_solve() << " seconds."<< std::endl;
		std::cout << "   Total elapsed wall time (assemble+solve): " << timer_assemble.wall_time()+timer_solve.wall_time() << " seconds."<< std::endl;    
	    
	    
		//quick save for time
		std::ofstream outfile1;    
	    
		outfile1.open("dedg2_time", std::ios_base::app); //wall time
		outfile1 << std::setprecision(16) << (timer_assemble.wall_time()+timer_solve.wall_time())  << std::endl;      
	    
	    timer_assemble.reset();  // reset timer for the next thing it shall do  
	    timer_solve.reset();     // reset timer for the next thing it shall do        
        
        
        error();
        //deallog << "Estimate " << estimate() << std::endl;
        output_results();
        std::cout << std::endl;
        std::cout << std::endl;
      }
  }
}



int main  (int argc, char **argv)
{
  try
    {
	  //here i delete results so that multiple runs do not append		
	  std::ofstream ofs;
	  ofs.open("dedg1_time", std::ofstream::out | std::ofstream::trunc);
	  ofs.close();		
	  ofs.open("dedg1_dos", std::ofstream::out | std::ofstream::trunc);
	  ofs.close();	  	  
	  ofs.open("dedg1_doa", std::ofstream::out | std::ofstream::trunc);
	  ofs.close();
	  ofs.open("dedg1_doados", std::ofstream::out | std::ofstream::trunc);
	  ofs.close();		
		
      using namespace dealii;
      using namespace Step39;

      deallog.depth_console(2);
      std::ofstream logfile("deallog");
      deallog.attach(logfile);
            
      //deal.ii bakes in MPI and PETSc initialization
      Utilities::MPI::MPI_InitFinalize mpi_initialization(argc, argv, 1);
      
      const int dim    = 2;  // Spatial dimension
      const int degree = 1;  // Polynomial order
      std::cout << "Solving problem in " << dim << " space dimensions." << std::endl;
      std::cout << std::endl;
      
      FE_DGQ< dim > fe1( degree ); 
      
      InteriorPenaltyProblem< dim > test1(fe1);
      test1.run( 5 );  //this gives 6 meshes 10x10,20x20,...,160x160
    }
  catch (std::exception &exc)
    {
      std::cerr << std::endl << std::endl
                << "----------------------------------------------------"
                << std::endl;
      std::cerr << "Exception on processing: " << std::endl
                << exc.what() << std::endl
                << "Aborting!" << std::endl
                << "----------------------------------------------------"
                << std::endl;
      return 1;
    }
  catch (...)
    {
      std::cerr << std::endl << std::endl
                << "----------------------------------------------------"
                << std::endl;
      std::cerr << "Unknown exception!" << std::endl
                << "Aborting!" << std::endl
                << "----------------------------------------------------"
                << std::endl;
      return 1;
    }

  return 0;
}
