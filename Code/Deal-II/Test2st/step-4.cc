/* ---------------------------------------------------------------------
 *
 * Copyright (C) 1999 - 2017 by the deal.II authors
 *
 * This file is part of the deal.II library.
 *
 * The deal.II library is free software; you can use it, redistribute
 * it, and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * The full text of the license can be found in the file LICENSE at
 * the top level of the deal.II distribution.
 *
 * ---------------------------------------------------------------------

 *
 * Author: Maurice S. Fabien, Rice University, 2017
 */


// @sect3{Include files}

// Bunch of include files, not all are needed as we are using PETSc
#include <deal.II/grid/tria.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/grid_tools.h>
#include <deal.II/grid/manifold_lib.h>
#include <deal.II/grid/grid_out.h>
#include <deal.II/grid/grid_in.h>

#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/fe/fe_q.h>
#include <deal.II/dofs/dof_tools.h>
#include <deal.II/fe/fe_values.h>
#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/function.h>
#include <deal.II/numerics/vector_tools.h>
#include <deal.II/numerics/matrix_tools.h>
#include <deal.II/lac/vector.h>
#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/sparse_matrix.h>             // Not needed as PETSc has its own matrix/vector containers
#include <deal.II/lac/dynamic_sparsity_pattern.h>  // Not needed as PETSc has its own matrix/vector containers
#include <deal.II/lac/solver_cg.h>                 // Not needed as PETSc has its own solvers
#include <deal.II/lac/precondition.h>              // Not needed as PETSc has its own preconditioners

//If parallelism is desired
//#include <deal.II/base/mpi.h>                        
//#include <deal.II/lac/petsc_parallel_sparse_matrix.h>
//#include <deal.II/lac/petsc_parallel_vector.h>

#include <deal.II/lac/petsc_sparse_matrix.h>   // Includes PETSc matrix containers
#include <deal.II/lac/petsc_vector.h>          // Includes PETSc vector containers

#include <deal.II/lac/petsc_solver.h>          // Includes PETSc solvers
#include <deal.II/lac/petsc_precondition.h>    // Includes PETSc preconditioner

#include <deal.II/numerics/data_out.h>

#include <deal.II/base/timer.h>

#include <fstream>
#include <iostream>


#include <deal.II/base/logstream.h>

// Import all the deal.II class and function names into the global namespace:
using namespace dealii;

// @sect3{The <code>Step4</code> class template}

// This is again the same <code>Step4</code> class as in the previous
// example. The only difference is that we have now declared it as a class
// with a template parameter, and the template parameter is of course the
// spatial dimension in which we would like to solve the Laplace equation. Of
// course, several of the member variables depend on this dimension as well,
// in particular the Triangulation class, which has to represent
// quadrilaterals or hexahedra, respectively. Apart from this, everything is
// as before.
template <int dim>
class Step4
{
public:
  Step4 ();
  void run ();

private:
  void make_grid ( int );
  void setup_system();
  void assemble_system ();
  void solve ();
  void output_results () const;

  Triangulation<dim>   triangulation;
  FE_Q<dim>            fe;
  DoFHandler<dim>      dof_handler;

  //If parallelism is needed
  //MPI_Comm mpi_communicator;
  //const unsigned int n_mpi_processes  = 1;
  //const unsigned int this_mpi_process = 0;


  PETScWrappers::SparseMatrix system_matrix;
  PETScWrappers::Vector       solution;
  PETScWrappers::Vector       system_rhs;

  /* //If you want to use the built-in deal-ii matrix/vector containers
  SparsityPattern      sparsity_pattern;
  SparseMatrix<double> system_matrix;

  Vector<double>       solution;
  Vector<double>       system_rhs;
  */
};


// @sect3{Right hand side and boundary values}

// In the following, we declare two more classes denoting the right hand side
// and the non-homogeneous Dirichlet boundary values. Both are functions of a
// dim-dimensional space variable, so we declare them as templates as well.
//
// Each of these classes is derived from a common, abstract base class
// Function, which declares the common interface which all functions have to
// follow. In particular, concrete classes have to overload the
// <code>value</code> function, which takes a point in dim-dimensional space
// as parameters and shall return the value at that point as a
// <code>double</code> variable.
//
// The <code>value</code> function takes a second argument, which we have here
// named <code>component</code>: This is only meant for vector valued
// functions, where you may want to access a certain component of the vector
// at the point <code>p</code>. However, our functions are scalar, so we need
// not worry about this parameter and we will not use it in the implementation
// of the functions. Inside the library's header files, the Function base
// class's declaration of the <code>value</code> function has a default value
// of zero for the component, so we will access the <code>value</code>
// function of the right hand side with only one parameter, namely the point
// where we want to evaluate the function. A value for the component can then
// simply be omitted for scalar functions.
//
// Note that the C++ language forces us to declare and define a constructor to
// the following classes even though they are empty. This is due to the fact
// that the base class has no default constructor (i.e. one without
// arguments), even though it has a constructor which has default values for
// all arguments.
template <int dim>
class RightHandSide : public Function<dim>
{
public:
  RightHandSide () : Function<dim>() {}

  virtual double value (const Point<dim>   &p,
                        const unsigned int  component = 0) const;
};



template <int dim>
class BoundaryValues : public Function<dim>
{
public:
  BoundaryValues () : Function<dim>() {}

  virtual double value (const Point<dim>   &p,
                        const unsigned int  component = 0) const;
};



template <int dim>
class Solution : public Function<dim>
{
public:
  Solution () : Function<dim>() {}

  virtual double value (const Point<dim>   &p,
                        const unsigned int  component = 0) const;
};



// The right hand side function is set as
// $8\pi^2\sin{2*\pi*x}\sin{2*\pi*y}$ in 2D,
// and $12\pi^2\sin{2*\pi*x}\sin{2*\pi*y}\sin{2*\pi*z}$ in 3D. 
//
// The last thing to note is that a <code>Point@<dim@></code> denotes a point
// in dim-dimensional space, and its individual components (i.e. $x$, $y$,
// ... coordinates) can be accessed using the () operator (in fact, the []
// operator will work just as well) with indices starting at zero as usual in
// C and C++.
template <int dim>
double RightHandSide<dim>::value (const Point<dim> &p,
                                  const unsigned int /*component*/) const
{
  double return_value = 0.0;
  double pi_squared   = (numbers::PI)*(numbers::PI);
  if (dim == 1){
    return_value = 4.0 * pi_squared * std::sin( 2.0 * numbers::PI * p(0) );
  }
  else if (dim == 2){
    return_value = 8.0 * pi_squared * std::sin( 2.0 * numbers::PI * p(0) ) * std::sin( 2.0 * numbers::PI * p(1) );
  }
  else if (dim == 3){
    return_value = 12.0 * pi_squared * std::sin( 2.0 * numbers::PI * p(0) ) * std::sin( 2.0 * numbers::PI * p(1) )* std::sin( 2.0 * numbers::PI * p(2) );
  }
  
  return return_value;
}


// As boundary values, we choose $\sin{2*\pi*x}\sin{2*\pi*y}$ in 2D, 
// and $\sin{2*\pi*x}\sin{2*\pi*y}\sin{2*\pi*z}$ in 3D. 
// Note this results in homogeneous Dirichlet boundary conditions.
template <int dim>
double BoundaryValues<dim>::value (const Point<dim> &p,
                                   const unsigned int /*component*/) const
{
  double return_value = 0.0;
  if (dim == 1){
    return_value = std::sin( 2.0 * numbers::PI * p(0) );
  }
  else if (dim == 2){
    return_value = std::sin( 2.0 * numbers::PI * p(0) ) * std::sin( 2.0 * numbers::PI * p(1) );
  }
  else if (dim == 3){
    return_value = std::sin( 2.0 * numbers::PI * p(0) ) * std::sin( 2.0 * numbers::PI * p(1) )* std::sin( 2.0 * numbers::PI * p(2) );
  }

  return return_value;
}

//The exact solution is $\sin{2*\pi*x}\sin{2*\pi*y}$ in 2D, 
// and $\sin{2*\pi*x}\sin{2*\pi*y}\sin{2*\pi*z}$ in 3D.
template <int dim>
double Solution<dim>::value (const Point<dim>   &p,
                               const unsigned int) const
{
  double return_value = 0.0;
  if (dim == 1){
    return_value = std::sin( 2.0 * numbers::PI * p(0) );
  }
  else if (dim == 2){
    return_value = std::sin( 2.0 * numbers::PI * p(0) ) * std::sin( 2.0 * numbers::PI * p(1) );
  }
  else if (dim == 3){
    return_value = std::sin( 2.0 * numbers::PI * p(0) ) * std::sin( 2.0 * numbers::PI * p(1) )* std::sin( 2.0 * numbers::PI * p(2) );
  }

  return return_value;
}


// @sect3{Implementation of the <code>Step4</code> class}

// Next for the implementation of the class template that makes use of the
// functions above. As before, we will write everything as templates that have
// a formal parameter <code>dim</code> that we assume unknown at the time we
// define the template functions. Only later, the compiler will find a
// declaration of <code>Step4@<2@></code> (in the <code>main</code> function,
// actually) and compile the entire class with <code>dim</code> replaced by 2,
// a process referred to as `instantiation of a template'. When doing so, it
// will also replace instances of <code>RightHandSide@<dim@></code> by
// <code>RightHandSide@<2@></code> and instantiate the latter class from the
// class template.
//
// In fact, the compiler will also find a declaration <code>Step4@<3@></code>
// in <code>main()</code>. This will cause it to again go back to the general
// <code>Step4@<dim@></code> template, replace all occurrences of
// <code>dim</code>, this time by 3, and compile the class a second time. Note
// that the two instantiations <code>Step4@<2@></code> and
// <code>Step4@<3@></code> are completely independent classes; their only
// common feature is that they are both instantiated from the same general
// template, but they are not convertible into each other, for example, and
// share no code (both instantiations are compiled completely independently).


// @sect4{Step4::Step4}

// After this introduction, here is the constructor of the <code>Step4</code>
// class. It specifies the desired polynomial degree of the finite elements
// and associates the DoFHandler to the triangulation just as in the previous
// example program, step-3:
template <int dim>
Step4<dim>::Step4 ()
  :
  fe (1), // fe(1) means linears, fe(2) quadratics, etc.  be carfeul, as quadrature rules need to be higher precision as well
  dof_handler (triangulation)
{}


// @sect4{Step4::make_grid}

// Grid specification.  The parameter structured_mesh handles the structured/unstructured mesh cases.
//  The structured mesh is created using deal-ii's built-in functions.
template <int dim>
void Step4<dim>::make_grid ( int refinement )
{
  int structured_mesh = 1; //0 means unstructured, 1 means structured.
  std::string s1 = "Test2_unstructuredquad";
  std::string s2 = std::to_string(refinement);
  std::string s3 = ".msh";
  std::string s4 = s1+s2+s3;
  if (structured_mesh == 0){

      GridIn<2> gridin; //notice I am assuming input mesh is 2D
      gridin.attach_triangulation(triangulation);
      std::ifstream f("Test2_unstructuredquad6.msh");  //s4
      gridin.read_msh(f);

//for whatever reason, we need to specify the boundary faces when importing the gmsh files
for (typename Triangulation<dim>::active_cell_iterator
       cell = triangulation.begin_active();
     cell != triangulation.end();
     ++cell)
  for (unsigned int f=0; f<GeometryInfo<dim>::faces_per_cell; ++f)
    if (cell->face(f)->at_boundary())
      if (cell->face(f)->center()[0] == 1)
        cell->face(f)->set_boundary_id (0);

for (typename Triangulation<dim>::active_cell_iterator
       cell = triangulation.begin_active();
     cell != triangulation.end();
     ++cell)
  for (unsigned int f=0; f<GeometryInfo<dim>::faces_per_cell; ++f)
    if (cell->face(f)->at_boundary())
      if (cell->face(f)->center()[0] == 0)
        cell->face(f)->set_boundary_id (0);

for (typename Triangulation<dim>::active_cell_iterator
       cell = triangulation.begin_active();
     cell != triangulation.end();
     ++cell)
  for (unsigned int f=0; f<GeometryInfo<dim>::faces_per_cell; ++f)
    if (cell->face(f)->at_boundary())
      if (cell->face(f)->center()[1] == 1)
        cell->face(f)->set_boundary_id (0);

for (typename Triangulation<dim>::active_cell_iterator
       cell = triangulation.begin_active();
     cell != triangulation.end();
     ++cell)
  for (unsigned int f=0; f<GeometryInfo<dim>::faces_per_cell; ++f)
    if (cell->face(f)->at_boundary())
      if (cell->face(f)->center()[1] == 0)
        cell->face(f)->set_boundary_id (0);

  }
  else
  {
    //GridGenerator::hyper_cube (triangulation, 0, 1);  //unit square (cube)
    if ( refinement == 1 ){
      const unsigned int  	repetitions = 10;  //forces a uniform mesh [0,1]^dim with repetitions^dim cells
      GridGenerator::subdivided_hyper_cube 	(triangulation,
	  	                                 repetitions,
	  	                                 0.0,
	  	                                 1.0);
    }
    else {
      triangulation.refine_global ( 1 );  //uniformly refine the mesh
    }

  }

  std::cout << "   Number of active cells: "
            << triangulation.n_active_cells()
            << std::endl
            << "   Total number of cells: "
            << triangulation.n_cells()
            << std::endl;
}

// @sect4{Step4::setup_system}

// This function looks exactly like in the previous example, although it
// performs actions that in their details are quite different if
// <code>dim</code> happens to be 3. The only significant difference from a
// user's perspective is the number of cells resulting, which is much higher
// in three than in two space dimensions!
template <int dim>
void Step4<dim>::setup_system ()
{
  dof_handler.distribute_dofs (fe);

  std::cout << "   Number of degrees of freedom: "
            << dof_handler.n_dofs()
            << std::endl;
 
  /* //If you want to use deal-ii default matrix/vector containers
  DynamicSparsityPattern dsp(dof_handler.n_dofs());
  DoFTools::make_sparsity_pattern (dof_handler, dsp);
  sparsity_pattern.copy_from(dsp);

  system_matrix.reinit (sparsity_pattern);
  solution.reinit (dof_handler.n_dofs());
  system_rhs.reinit (dof_handler.n_dofs());
  */

 system_matrix.reinit(dof_handler.n_dofs(),
                      dof_handler.n_dofs(),
                      dof_handler.max_couplings_between_dofs() );

  solution.reinit ( dof_handler.n_dofs() );
  system_rhs.reinit ( dof_handler.n_dofs() );


}


// @sect4{Step4::assemble_system}

// Unlike in the previous example, we would now like to use a non-constant
// right hand side function and non-zero boundary values. Both are tasks that
// are readily achieved with only a few new lines of code in the assemblage of
// the matrix and right hand side.
//
// More interesting, though, is the way we assemble matrix and right hand side
// vector dimension independently: there is simply no difference to the
// two-dimensional case. Since the important objects used in this function
// (quadrature formula, FEValues) depend on the dimension by way of a template
// parameter as well, they can take care of setting up properly everything for
// the dimension for which this function is compiled. By declaring all classes
// which might depend on the dimension using a template parameter, the library
// can make nearly all work for you and you don't have to care about most
// things.
template <int dim>
void Step4<dim>::assemble_system ()
{
  QGauss<dim>  quadrature_formula(2); //p=1,q=2

  // We wanted to have a non-constant right hand side, so we use an object of
  // the class declared above to generate the necessary data. Since this right
  // hand side object is only used locally in the present function, we declare
  // it here as a local variable:
  const RightHandSide<dim> right_hand_side;

  // Compared to the previous example, in order to evaluate the non-constant
  // right hand side function we now also need the quadrature points on the
  // cell we are presently on (previously, we only required values and
  // gradients of the shape function from the FEValues object, as well as the
  // quadrature weights, FEValues::JxW() ). We can tell the FEValues object to
  // do for us by also giving it the #update_quadrature_points flag:
  FEValues<dim> fe_values (fe, quadrature_formula,
                           update_values   | update_gradients |
                           update_quadrature_points | update_JxW_values);

  // We then again define a few abbreviations. The values of these variables
  // of course depend on the dimension which we are presently using. However,
  // the FE and Quadrature classes do all the necessary work for you and you
  // don't have to care about the dimension dependent parts:
  const unsigned int   dofs_per_cell = fe.dofs_per_cell;
  const unsigned int   n_q_points    = quadrature_formula.size();

  FullMatrix<double>   cell_matrix (dofs_per_cell, dofs_per_cell);
  Vector<double>       cell_rhs (dofs_per_cell);

  std::vector<types::global_dof_index> local_dof_indices (dofs_per_cell);

  // Next, we again have to loop over all cells and assemble local
  // contributions.  Note, that a cell is a quadrilateral in two space
  // dimensions, but a hexahedron in 3D. In fact, the
  // <code>active_cell_iterator</code> data type is something different,
  // depending on the dimension we are in, but to the outside world they look
  // alike and you will probably never see a difference although the classes
  // that this typedef stands for are in fact completely unrelated:
  typename DoFHandler<dim>::active_cell_iterator
  cell = dof_handler.begin_active(),
  endc = dof_handler.end();

  for (; cell!=endc; ++cell)
    {
      fe_values.reinit (cell);
      cell_matrix = 0;
      cell_rhs = 0;

      // Now we have to assemble the local matrix and right hand side. This is
      // done exactly like in the previous example, but now we revert the
      // order of the loops (which we can safely do since they are independent
      // of each other) and merge the loops for the local matrix and the local
      // vector as far as possible to make things a bit faster.
      //
      // Assembling the right hand side presents the only significant
      // difference to how we did things in step-3: Instead of using a
      // constant right hand side with value 1, we use the object representing
      // the right hand side and evaluate it at the quadrature points:
      for (unsigned int q_index=0; q_index<n_q_points; ++q_index)
        for (unsigned int i=0; i<dofs_per_cell; ++i)
          {
            for (unsigned int j=0; j<dofs_per_cell; ++j)
              cell_matrix(i,j) += (fe_values.shape_grad (i, q_index) *
                                   fe_values.shape_grad (j, q_index) *
                                   fe_values.JxW (q_index));

            cell_rhs(i) += (fe_values.shape_value (i, q_index) *
                            right_hand_side.value (fe_values.quadrature_point (q_index)) *
                            fe_values.JxW (q_index));
          }
      // As a final remark to these loops: when we assemble the local
      // contributions into <code>cell_matrix(i,j)</code>, we have to multiply
      // the gradients of shape functions $i$ and $j$ at point number
      // q_index and
      // multiply it with the scalar weights JxW. This is what actually
      // happens: <code>fe_values.shape_grad(i,q_index)</code> returns a
      // <code>dim</code> dimensional vector, represented by a
      // <code>Tensor@<1,dim@></code> object, and the operator* that
      // multiplies it with the result of
      // <code>fe_values.shape_grad(j,q_index)</code> makes sure that the
      // <code>dim</code> components of the two vectors are properly
      // contracted, and the result is a scalar floating point number that
      // then is multiplied with the weights. Internally, this operator* makes
      // sure that this happens correctly for all <code>dim</code> components
      // of the vectors, whether <code>dim</code> be 2, 3, or any other space
      // dimension; from a user's perspective, this is not something worth
      // bothering with, however, making things a lot simpler if one wants to
      // write code dimension independently.

      // With the local systems assembled, the transfer into the global matrix
      // and right hand side is done exactly as before, but here we have again
      // merged some loops for efficiency:
      cell->get_dof_indices (local_dof_indices);
      for (unsigned int i=0; i<dofs_per_cell; ++i)
        {
          for (unsigned int j=0; j<dofs_per_cell; ++j)
            system_matrix.add (local_dof_indices[i],
                               local_dof_indices[j],
                               cell_matrix(i,j));

          system_rhs(local_dof_indices[i]) += cell_rhs(i);
        }
    }


  // As the final step in this function, we wanted to have non-homogeneous
  // boundary values in this example, unlike the one before. This is a simple
  // task, we only have to replace the ZeroFunction used there by an object of
  // the class which describes the boundary values we would like to use
  // (i.e. the <code>BoundaryValues</code> class declared above):

  system_matrix.compress(VectorOperation::add);
  system_rhs.compress(VectorOperation::add);

  std::map<types::global_dof_index,double> boundary_values;
  VectorTools::interpolate_boundary_values (dof_handler,
                                            0,
                                            BoundaryValues<dim>(),
                                            boundary_values);
  MatrixTools::apply_boundary_values (boundary_values,
                                      system_matrix,
                                      solution,
                                      system_rhs,
				      false);
}


// @sect4{Step4::solve}

// Solving the linear system of equations.
template <int dim>
void Step4<dim>::solve ()
{
/*  //deal-ii solver and preconditioner
  SolverControl           solver_control (1000, 1e-7);//MSF:  1000 maximum iterations, CG tolernace of 1e-7
  SolverCG<>              solver (solver_control);

  //  SSOR Preconditioner definition and step-up!
  PreconditionSSOR<> preconditioner;
  preconditioner.initialize(system_matrix, 1.2);
  solver.solve (system_matrix, solution, system_rhs,
                preconditioner);
*/

    //PETSc BoomerAMG + CG solver

    SolverControl           solver_control (solution.size(), 1e-7*system_rhs.l2_norm());
    //SolverControl           solver_control (10000, 1e-7*system_rhs.l2_norm());

    //SolverControl           solver_control (solution.size(), 1e-7);
    PETScWrappers::SolverCG cg (solver_control);

    //PETScWrappers::PreconditionBlockJacobi preconditioner(system_matrix);

    PETScWrappers::PreconditionBoomerAMG preconditioner;
    PETScWrappers::PreconditionBoomerAMG::AdditionalData data;
    preconditioner.initialize(system_matrix, data);

    cg.solve (system_matrix, solution, system_rhs,
    	      preconditioner);

    //data.symmetric_operator = true;
    //preconditioner.initialize(system_matrix, data);
    //solver.solve (system_matrix, solution, system_rhs,          preconditioner);

/* No preconditioner!
  solver.solve (system_matrix, solution, system_rhs,
                PreconditionIdentity());
*/

  // We have made one addition, though: since we suppress output from the
  // linear solvers, we have to print the number of iterations by hand.
  std::cout << "   " << solver_control.last_step()
            << " CG iterations needed to obtain convergence."
            << std::endl;

}


// @sect4{Step4::output_results}

// Output results if wanted.
template <int dim>
void Step4<dim>::output_results () const
{

  Vector<double> difference_per_cell (triangulation.n_active_cells());
  VectorTools::integrate_difference (dof_handler,
                                   solution,
                                   Solution<dim>(),
                                   difference_per_cell,
                                   QGauss<dim>(3),
                                   VectorTools::L2_norm);
  const double L2_error = difference_per_cell.l2_norm();

  double DoA = -1.0*(std::log10(L2_error));
  double DoS = std::log10(dof_handler.n_dofs());

  std::cout << "   " << "L2-Norm error = " << L2_error                         << std::endl; 
  std::cout << "   " << "DoS           = " << DoS << std::endl;

  std::cout << "   " << "DoA           = " << DoA      << std::endl;
  std::cout << "   " << "DoA/DoS       = " << DoA/DoS << std::endl;
  
    
  //quick save for DoA, DoA/DoS
  std::ofstream outfile1, outfile2;
  
  outfile1.open("detest2st_doa", std::ios_base::app);
  outfile1 << std::setprecision(16) << DoA << std::endl;   
  
  outfile2.open("detest2st_doados", std::ios_base::app);
  outfile2 << std::setprecision(16) << DoA/DoS << std::endl;   
  
  
  /*
  DataOut<dim> data_out;

  data_out.attach_dof_handler (dof_handler);
  data_out.add_data_vector (solution, "solution");

  data_out.build_patches ();

  std::ofstream output (dim == 2 ?
                        "solution-2d.vtk" :
                        "solution-3d.vtk");
  data_out.write_vtk (output);
  */
}



// @sect4{Step4::run}

// Function driver.
template <int dim>
void Step4<dim>::run ()
{
  //here i delete results so that multiple runs do not append
  std::ofstream ofs;
  ofs.open("detest2st_time", std::ofstream::out | std::ofstream::trunc);
  ofs.close();		
  ofs.open("detest2st_doa", std::ofstream::out | std::ofstream::trunc);
  ofs.close();
  ofs.open("detest2st_doados", std::ofstream::out | std::ofstream::trunc);
  ofs.close();
	
  std::cout << "Solving problem in " << dim << " space dimensions." << std::endl;

  int refinement;
  int max_refinement = 6;
  
  Timer timer_assemble;
  Timer timer_solve;
  
  for (refinement = 1; refinement <= max_refinement; refinement++){
    make_grid( refinement );
    setup_system ();
    
	timer_assemble.start ();        
    assemble_system ();
    timer_assemble.stop ();
         
    timer_solve.start();
    solve ();
    timer_solve.stop();
       
    output_results ();  //error, DoA, DoS, DoA/DoS
    
	std::cout << "   Elapsed CPU time (assemble): " << timer_assemble() << " seconds."<< std::endl;
	std::cout << "   Elapsed wall time (assemble): " << timer_assemble.wall_time() << " seconds."<< std::endl;
	
	std::cout << "   Elapsed CPU time (solve): " << timer_solve() << " seconds."<< std::endl;
	std::cout << "   Elapsed wall time (solve): " << timer_solve.wall_time() << " seconds."<< std::endl;	          
    
	std::cout << "   Total elapsed CPU time (assemble+solve): " << timer_assemble()+timer_solve() << " seconds."<< std::endl;
	std::cout << "   Total elapsed wall time (assemble+solve): " << timer_assemble.wall_time()+timer_solve.wall_time() << " seconds."<< std::endl;    
    
    
	//quick save for time
	std::ofstream outfile1;    
    
	outfile1.open("detest2st_time", std::ios_base::app); //wall time
	outfile1 << std::setprecision(16) << (timer_assemble.wall_time()+timer_solve.wall_time())  << std::endl;      
    
    timer_assemble.reset();  // reset timer for the next thing it shall do  
    timer_solve.reset();     // reset timer for the next thing it shall do
    
    std::cout << std::endl;
  }

}


// @sect3{The <code>main</code> function}
// Main
int main (int argc, char **argv)
{
  deallog.depth_console (0);
  {
    Utilities::MPI::MPI_InitFinalize mpi_initialization(argc, argv, 1);
    Step4<2> laplace_problem_2d;
    laplace_problem_2d.run ();
  }
  /*{ // 3D
    Utilities::MPI::MPI_InitFinalize mpi_initialization(argc, argv, 1);
    Step4<3> laplace_problem_3d;
    laplace_problem_3d.run ();
  }*/

  return 0;
}
