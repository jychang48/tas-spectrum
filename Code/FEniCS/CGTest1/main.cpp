#include <dolfin.h>
#include "CG.h"
#include "L2error.h"
#include <math.h>

using namespace dolfin;

class Source : public Expression
{
  void eval(Array<double>& values, const Array<double>& x) const
  {
    values[0] = 4*M_PI*((x[0]*x[0]+x[1]*x[1])*4*M_PI*sin(2*M_PI*x[0]*x[0])*sin(2*M_PI*x[1]*x[1]) - 
        cos(2*M_PI*x[0]*x[0])*sin(2*M_PI*x[1]*x[1]) - sin(2*M_PI*x[0]*x[0])*cos(2*M_PI*x[1]*x[1]));
  }
};

class ExactSolution : public Expression
{
  void eval(Array<double>& values, const Array<double>& x) const
  {
    values[0] = sin(2*M_PI*x[0]*x[0])*sin(2*M_PI*x[1]*x[1]);
  }
};

class DirichletBoundary : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return x[0] < DOLFIN_EPS or x[0] > 1.0 - DOLFIN_EPS or x[1] < DOLFIN_EPS or x[1] > 1.0 - DOLFIN_EPS or x[2] < DOLFIN_EPS or x[2] > 1.0 - DOLFIN_EPS;
  }
};

int main(int argc, char* argv[])
{
  // Initialize stuff
  double tstart,tend;
  int rank, size;
  int seed;
  seed = atoi(argv[1]);
 
  // Create mesh and function space
  auto mesh = std::make_shared<UnitSquareMesh>(seed,seed);
  auto V = std::make_shared<CG::FunctionSpace>(mesh);  
  MPI_Comm_size(MPI_COMM_WORLD,&size);
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  // Define boundary condition
  auto u0 = std::make_shared<ExactSolution>();
  auto boundary = std::make_shared<DirichletBoundary>();
  auto bc = std::make_shared<DirichletBC>(V, u0, boundary);

  // Define variational forms
  CG::BilinearForm a(V, V);
  CG::LinearForm L(V);
  auto f = std::make_shared<Source>();
  L.f = f;

  // Define linear algebra
  auto A = std::make_shared<Matrix>();
  Vector b;
  KrylovSolver solver("cg", "hypre_amg");
  solver.parameters["relative_tolerance"] = 1.0e-7;
  PETScOptions::set("ksp_converged_reason");
  auto u = std::make_shared<Function>(V);

  // Show problem size
  auto usize = u->vector();
  if (!rank) {
    PetscPrintf(MPI_COMM_WORLD,"\tDegrees-of-freedom: %d\n", usize->size());
  }

  // Compute solution
  tstart = MPI_Wtime();
  assemble_system(*A, b, a, L, {bc});
  solver.set_operators(A,A);
  solver.solve(*u->vector(),b);
  tend = MPI_Wtime();

  // Compute L2 error
  L2error::Functional L2_errorform(mesh);
  L2_errorform.ExactSolution = u0;
  L2_errorform.ComputedSolution = u;
  double l2errornorm = sqrt(assemble(L2_errorform));

  // Show time-to-solution
  if (!rank) {
    double doa = -log10(l2errornorm);
    PetscPrintf(MPI_COMM_WORLD,"\tWall-clock time: %1.3e seconds\n", tend-tstart);
    PetscPrintf(MPI_COMM_WORLD,"\tL2 error norm: %1.3e\n", l2errornorm);
    PetscPrintf(MPI_COMM_WORLD,"\tDigit of accuracy: %1.3e\n", doa);
    PetscPrintf(MPI_COMM_WORLD,"\tDoA/s: %1.3e\n", doa/(tend-tstart));
    PetscPrintf(MPI_COMM_WORLD,"\tDoF/s: %1.3e\n", usize->size()/(tend-tstart));

  }

  // Save solution in VTK format
  //File file("poisson.pvd");
  //file << u;

  return 0;
}
