\section{Theoretical Analysis}\label{sec:theory}
\begin{figure}[t]
  \centering
  \subfloat{\includegraphics[width=0.9\textwidth]{example_static_diagram.pdf}}
  \caption{Example static-scaling diagram and a description of the behavioral characteristics. This diagram
  is able to characterize both strong-scaling and weak-scaling effects across a variety of problem sizes.
  \label{Fig:examplestatic}}
\end{figure}
\begin{figure}[t]
  \centering
  \subfloat[$d = 2$]{\includegraphics[width=0.5\textwidth]{exampledoe2d.pdf}}
  \subfloat[$d = 3$]{\includegraphics[width=0.5\textwidth]{exampledoe3d.pdf}}
  \caption{Example DoE diagrams for 2D and 3D problems assuming $C = 10$, $W = 0.1$ and that
  $h$-sizes range from 1/10 to 1/5120. Both $\alpha$ and $d$ have a drastic effect on the slope of 
  these lines. \label{Fig:exampledoe}}
\end{figure}

Unlike the mesh convergence and static-scaling analyses, the accuracy 
rate diagrams, which consist of the DoE and the true DoF per time metrics, measure 
the accuracy achieved by a particular method in a 
given amount of time. In this Section, we will discuss the theoretical 
underpinning of these two diagrams and examine the behavior of the line plots.
The DoE is written as:
\begin{align}
\mathrm{DoE}\ = -\log_{10}(err\times T).
\end{align}
Recall that $err \le C h^\alpha$ is the $L_2$ norm of the error with a theoretical convergence
rate $\alpha$, which can be obtained directly using MMS, and $T$ is the 
{\color{red}measured wallclock time}:
\begin{align}
  T = W h^{-d},
\end{align}
where $d$ is the spatial dimension, $h$ denotes the representative element length, and 
$C$ and $W$ are constants. Then for a given run, the digits of efficacy would be given by
\begin{align}
  \mathrm{DoE} &= -\log_{10}\left( C h^\alpha W h^{-d} \right) \\
               &= -\log_{10}\left( C W h^{\alpha - d} \right) \\
               &= (d - \alpha) \log_{10}(h) - \log_{10}(C W).
\end{align}
Since $C$ and $W$ are constants, the slopes of the DoE lines are only affected by $\alpha$ and $d$.
However, because of strong-scaling and weak-scaling limits, it is possible that the time $T$ may not always be
of linear complexity thus the slope may not actually be $d-\alpha$. Let us consider a simple static-scaling
example shown in Figure \ref{Fig:examplestatic}. It can be seen here that the DoF per time (or $N/T$) ratios 
indicate at what points both strong-scaling and weak-scaling/memory effects start to dominate for a given 
MPI parallelism. If $C = 10$, $W = 0.1$, and $h$-sizes ranging from 1/10 to 1/5120, we can see from 
Figure \ref{Fig:exampledoe} what type of slopes we could expect to see in the DoE diagrams assuming the
same $T$ from Figure \ref{Fig:examplestatic}. It can be seen that methods with higher order rates of convergence
are preferable as $h$ is refined. For this particular example's chosen parameters, it can also be 
seen that the strong-scaling effects skew a few of the data points at the beginning but the 
weak-scaling effects are nearly unseen. Such effects may not always be negligible in these DoE diagrams but could be carefully noted from static-scaling.

\begin{figure}[t]
  \centering
  \subfloat[$h$-size range: 10$^{-1}$ to 10$^{-3}$]{\includegraphics[width=0.5\textwidth]{exampletrue0.pdf}}
  \subfloat[$h$-size range: 10$^{-2}$ to 10$^{-4}$]{\includegraphics[width=0.5\textwidth]{exampletrue1.pdf}}\\
  \subfloat[$h$-size range: 10$^{-3}$ to 10$^{-5}$]{\includegraphics[width=0.5\textwidth]{exampletrue2.pdf}}
  \subfloat[$h$-size range: 10$^{-5}$ to 10$^{-7}$]{\includegraphics[width=0.5\textwidth]{exampletrue4.pdf}}\\
  \subfloat[$h$-size range: 10$^{-7}$ to 10$^{-9}$]{\includegraphics[width=0.5\textwidth]{exampletrue6.pdf}}
  \subfloat[$h$-size range: 10$^{-10}$ to 10$^{-12}$]{\includegraphics[width=0.5\textwidth]{exampletrue9.pdf}}
  \caption{True DoF per time rates in comparison to the {\color{red}standard} DoF per time rate for various $h$-sizes and $\alpha$. Let $d=2$, $C=10$, $D=4$, and the optimal DoF/$T = 4\times10^{5}$.\label{Fig:exampletruedof}}
\end{figure}
In true static-scaling, the DoF per time metric needs to be
scaled by the mesh convergence ratio DoA/DoS. Before we get into the
analysis of this scaling plot, we list a few key assumptions that must be
made in order for this to work:
\begin{enumerate}
\item Problem size DoF or $N = Dh^{-d}$ where $D$ is a constant. 
\item All computations are of linear complexity i.e., $\mathcal{O}(N)$.
\item Any tailing off from $\mathcal{O}(N)$ occurs due to  hardware related 
issues like latency from strong-scaling effects, memory bandwidth 
contention, or cache misses.
\item The problem size $N$ or DoF must be greater than 1.
\item $L_2$ error norm $err < 1.0$.
\end{enumerate}
Any violations to the last two assumptions would require that a different
logarithm rule be used or for the numbers to be scaled so that neither DoA nor DoS
are zero. The behavior of the true static-scaling line plot is given by:
\begin{align}
\left(\frac{\mathrm{DoA}}{\mathrm{DoS}}\right)\times\left(\frac{N}{T}\right) 
&= \left(\frac{-\log_{10}(C h^\alpha)}{\log_{10}(D h^{-d})}\right)\times\left(\frac{Dh^{-d}}{Wh^{-d}}\right) \\
&= \frac{-\log_{10}(C)-\log_{10}(h^\alpha)}{\log_{10}(D) + \log_{10}(h^{-d})}\times\left(\frac{D}{W}\right) \\
&= \frac{-\alpha\log_{10}(h) - \log_{10}(C)}{-d \log_{10}(h) + \log_{10}(D)}\times\left(\frac{D}{W}\right).
\end{align}
The variables $\alpha$, $d$, $C$, and $D$ will significantly impact the qualitative behavior of the true static-scaling
diagrams. As $h$ approaches zero, the $N/T$ ratio will slowly asymptote to a new $N/T$ ratio scaled by the factor
$\alpha/d$. Consider the following true-static scaling diagrams in Figure \ref{Fig:exampletruedof} when $d=2$, $C = 10$,
$D = 4$, and a variety of $h$-sizes are examined. It can be seen here that for larger $h$-sizes or coarser meshes, 
the DoF/$T$ lines are drastically skewed, and the optimal scaling regions are no longer horizontal. The 
relative ordering of the line plots in both accuracy rate diagrams depend significantly on the constants 
$C$, $D$, and $W$.

