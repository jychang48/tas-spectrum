\section{Conclusion}\label{sec:conclusion}

By incorporating a measure of accuracy, or convergence of the numerical 
method, into performance analysis metrics, we are able to make meaningful 
performance comparisons between different finite element methods for 
which the worth of an individual FLOP differs due to 
different approximation properties of the algorithm---whether because of 
differences in discretizations, convergence rates, or any 
any other reason. For example, we saw that for the 3D Poisson problem 
with smooth coefficients, the DG1 method may have the highest computation
rate in terms of DoF per time but have low DoE and true DoF per time metrics 
once DoA is taken into consideration. Simultaneously looking at the DoE
and true DoF per time diagrams can further the understanding of how fast
and accurate a particular method is. 
\subsection{Extensions of this work}
The Time-Accuracy-Size (TAS) spectrum analysis opens the door to a variety 
of possible performance analyses. The most logical extension of this work 
would be to to analyze different and more complicated PDEs, but there still exist
some important issues that were not covered in this paper. We now 
briefly highlight some of these important areas of future research:
\begin{itemize}
\item \textit{Arithmetic intensity:} A logical extension of the TAS spectrum 
performance analysis would be to incorporate the Arithmetic Intensity (AI), used in the 
performance spectrum \cite{ChangNakshatralaKnepleyJohnsson2017} and 
roofline performance model \cite{Williams_ACM_2009}. The AI of an algorithm or software is a 
measure that aids in estimating how efficiently the hardware resources and 
capabilities can be utilized. The limiting factor of performance 
for many PDE solvers is the memory bandwidth, so having a high AI
increases the possibility of reusing more data in cache and lowers memory bandwidth 
demands. It can be measured in a number of ways, such as through the 
Intel SDE/VTune libraries or through hardware counters like cache misses.
% RTM: Again, removed registered/trademark symbols -- not appropriate for journal articles, generally.
\item \textit{Numerical discretization:} This paper has solely focused on the 
finite element method using CG and DG discretization, so it would be a worthy
research endeavor to investigate other types of elements like hybrid or mixed elements. 
Furthermore, this type of analysis is easily extendible to other numerical methods like 
the finite difference, finite volume, spectral element, and boundary integral methods.
\item \textit{Accuracy measures:} The accuracy rate metrics were based on the $L_2$ error norm,
but other measures of accuracy or convergence, like the $H^1$ error seminorm, can be used.
The accuracy of different numerical methodologies may sometimes require more than just the standard
$L_2$ error norm. For example, one can validate and verify the performance of finite element methods
for porous media flow models using the mechanics-based solution verification measures described
in \cite{shabouei2016mechanics}.
\item \textit{Solver strategies:} Only the multigrid solvers HYPRE, GAMG, and ML have been
used for the experiments in this paper but there are various other solver and preconditioning strategies 
one can use which may drastically alter the comparisons between the CG and DG
methods. A thorough analysis and survey of all the appropriate solver and preconditioning combinations
may be warranted for any concrete conclusions to be made about these finite element methods. 
There are also different ways of enforcing constraints or conservation laws, solving nonlinear systems with 
hybrid and composed iterations, and handling coefficient jumps in different ways. 
For this, we may also want to incorporate statistics of the
iteration involved~\cite{MorganKnepleySananScott2016}. 
\item \textit{Large-scale simulations:} The computational experiments performed in the previous section
are relatively small, but they can easily scale up so that up to 100K or more MPI processes may be needed.
Furthermore, even if smaller scale comparisons were to be made such as the ones shown in 
this paper, the choice of hardware architecture could play an important role in the scaling analyses.
Intel systems were used to convey some important performance comparisons, but such comparisons
may be very different on systems provided by IBM, AMD, or even NVIDIA.
\end{itemize}
