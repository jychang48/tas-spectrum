\section{Experimental Setup}\label{sec:setup}

In this paper, we only consider the Poisson equation,
\begin{align}\label{Eqn:model_problem}
  -\nabla^{2} u &= f,   &u &\in \Omega\\
  u &= u_0, &u &\in \Gamma_{D}
\end{align}
where $\Omega$ denotes the computational domain in $\mathbb{R}^d$, $\Gamma_D$ denotes its boundary, $u$ is the
scalar solution field, and $u_0$ are the prescribed Dirichlet boundary values.

The finite element discretizations considered are the Continuous Galerkin (CG) and Discontinuous Galerkin (DG)
methods. Various levels of both $h$- and $p$-refinement for the CG and DG methods are considered across different
software implementations. We do not consider other viable approaches such as the Hybridized Discontinuous Galerkin
(HDG) method~\cite{kirby2012cg,FabienKnepleyRiviere2017} or mixed formulations~\cite{raviart1977mixed,
cockburn2009unified} but these will be addressed in future work. To this end, let us define $E$ as
an element belonging to a mesh $\mathcal{E}(\Omega)$. The relevant finite-dimensional function space for simplices is
\begin{align}
  \mathcal{U}_h &:= \left\{u_h \in L^{2}(\Omega):\; u_h\big|_E \in \mathcal{P}_{p}(E) \quad\forall\ E \in \mathcal{E}(\Omega)\right\},
\end{align}
and for tensor product cells is
\begin{align}
  \mathcal{U}_h &:= \left\{u_h \in L^{2}(\Omega):\; u_h\big|_E \in \mathcal{Q}_{p}(E) \quad\forall\ E \in \mathcal{E}(\Omega)\right\}.
\end{align}
Here $\mathcal{P}_{p}(E)$ denotes the space of polynomials in $d$ variables of degree less than or equal to $p$ over the
element $E$, and $\mathcal{Q}_{p}(E)$ is the space of $d$-dimensional tensor products of polynomials of degree less than
or equal to $p$. The general form of the weak formulation for equation \eqref{Eqn:model_problem} can be written as
follows: Find $u_h\in\mathcal{U}_h$ such that
\begin{align}\label{Eqn:weak_form_general}
  \mathcal{B}\left(v_h; u_h\right) = \mathcal{L}\left(v_h\right) \quad \forall\ v_h\in\mathcal{U}_h
\end{align}
where $\mathcal{B}$ and $\mathcal{L}$ denote the bilinear and linear forms, respectively.

\subsection{Finite element discretizations}

For the CG discretization, the solution $u_h$ is continuous at element boundaries, so that $\mathcal{U}_h$ is actually a
subspace of $H^1$, and the test functions satisfy $v_h = u_0$ on $\Gamma_D$. To present the DG formulation
employed in the paper, we introduce some notation. The boundary of a cell $E_i$ is denoted by $\partial E_i$. The
interior face between $E_i$ and $E_j$ is denoted by $\Gamma_{ij}$. That is,
\begin{align}
  \Gamma_{ij} = \partial E_i \cap \partial E_j
\end{align}
The set of all points on the interior faces is denoted by $\Gamma_{\mathrm{int}}$. Mathematically,
\begin{align}
  \Gamma_{\mathrm{int}} = \bigcup_{i,j}^{\mathcal{E}(\Omega)} \Gamma_{ij}
\end{align}
For an interior face, we denote the subdomains shared by this face by $E^{+}$ and $E^{-}$. The outward normals on this
face for these cells are, respectively, denoted by $\widehat{\mathbf{n}}^{+}$ and $\widehat{\mathbf{n}}^{-}$.
%
Employing Brezzi's notation~\cite{ArnoldBrezziCockburnMarini2002}, the average and jump operators on an interior face
are defined as follows
\begin{align}
  \big\{c\big\} := \frac{c^+ + c^-}{2} \quad \mathrm{and} \quad
  \big[\!\big[c\big]\!\big] := c^+ \widehat{\mathbf{n}}^+ + c^- \widehat{\mathbf{n}}^-
\end{align}
where
\begin{align}
  c^+ = c\vert_{\partial E^+} \quad \mathrm{and} \quad c^- = c\vert_{\partial E^-}
\end{align}
Let $  \Gamma_D$ denote the set of all boundary faces.  For a face $e\in \Gamma_D$, we then define $\{ c \} = c|_e  $, and $[\![c]\!] = c|_e \widehat{\mathbf{n}}_e.$  One of the most popular DG formulations is the \emph{Symmetric Interior Penalty} method, which for equation
\eqref{Eqn:weak_form_general} is written
%%Old formulation:
%\begin{align}
%  \mathcal{B}(v_h;u_h) &:= \Big(\nabla v_h;\;\nabla u_h\Big)_\Omega
%%<<<<<<< HEAD
%  - \Big(\big[\!\big[v_h\big]\!\big];\;\big\{\nabla u_h\big\}\Big)_{\Gamma_\mathrm{int}\cup\Gamma_D} - \Big(\big\{\nabla v_h\big\};\;\big[\!\big[u_h\big]\!\big]\Big)_{\Gamma_\mathrm{int}\cup\Gamma_D} \nonumber \\
%& + \alpha\Big(\big[\!\big[v_h\big]\!\big];\;\big[\!\big[u_h\big]\!\big]\Big)_{\Gamma_\mathrm{int}} + \gamma\Big(v_h;\;u_h\Big)_{\Gamma_D}\\
%  \mathcal{L}(v_h) &:= \Big(v_h;\;f \Big)_\Omega - \Big(\nabla v_h\cdot\widehat{\mathbf{n}}_{e};\;u_0\Big)_{\Gamma_D} 
%  + \gamma\Big(v_h;\;u_0\Big)_{\Gamma_D}
%%=======
%%  - \Big(\big[\!\big[v_h\big]\!\big];\;\big\{\nabla u_h\big\}\Big)_{\Gamma_\mathrm{int} \cup \Gamma_D} \nonumber \\
%% &- \Big(\big\{\nabla v_h\big\};\;\big[\!\big[u_h\big]\!\big]\Big)_{\Gamma_\mathrm{int} \cup \Gamma_D}
%%  +  \Big(\alpha \big[\!\big[v_h\big]\!\big];\;\big[\!\big[u_h\big]\!\big]\Big)_{\Gamma_\mathrm{int} \cup \Gamma_D}
%%  \\
%%  \mathcal{L}(v_h) &:= \Big(v_h;\;f \Big)_\Omega - \Big(\nabla v_h\cdot\widehat{\mathbf{n}}_{e};\;u_0\Big)_{  \Gamma_D} 
%%  -  \Big( \gamma v_h;\;u_0\Big)_{  \Gamma_D}
%%>>>>>>> 0589abeccd7986e125c15aa8d10f9e098c2df95b
%\end{align}
\begin{align}
  \mathcal{B}(v_h;u_h) 
  &:= 
  \sum_{E \in \mathcal{E}(\Omega) } \int_E \nabla v_h \cdot \nabla u_h 
  -
  \sum_{e \in \Gamma_\mathrm{int} \cup \Gamma_D} \int_e \{ v_h \} \cdot \big[\!\big[u_h\big]\!\big]
  -
  \sum_{e \in \Gamma_\mathrm{int} \cup \Gamma_D} \int_e \{ u_h \} \cdot \big[\!\big[v_h\big]\!\big]
  \nonumber 
  \\ 
  &+
  \sigma
  \sum_{e \in \Gamma_\mathrm{int}  } \int_e \frac{|e|}{|E|} \big[\!\big[v_h\big]\!\big]  \cdot \big[\!\big[u_h\big]\!\big]  
  + 
  \gamma
  \sum_{e \in \Gamma_D } \int_e \frac{|e|}{|E|}  v_h u_h
  \\
  \mathcal{L}(v_h) &:= \sum_{E \in \mathcal{E}(\Omega) } \int_E v_h f
  -
  \sum_{e \in \Gamma_\mathrm{D}  } \int_e u_0 \nabla v_h \cdot \widehat{\mathbf{n}}_{e}
  +
  \gamma
  \sum_{e \in \Gamma_\mathrm{D}  } \int_e \frac{|e|}{|E|} v_h u_0
\end{align}
where $\widehat{\mathbf{n}}_{e}$ denotes the outward normal on an exterior face, $|e|$ is the measure of a face in the given triangulation, $|E|$ is the measure of a cell in the given triangulation, and the penalty terms $\alpha$
and $\gamma$ are written as:
\begin{align}
\sigma &= \frac{(p + 1)(p + d)}{2d}\\
\gamma &= 2\alpha
\end{align}
as described in ~\cite{Shahbazi2005}.

%\begin{figure}[t]
%  \centering
%  \subfloat{\includegraphics[height=0.45\textheight]{meshes.pdf}}
%  \caption{The structured and unstructured grids considered in this paper.\label{Fig:meshes}}
%\end{figure}
\begin{figure}[t]
  \centering
  \subfloat{\includegraphics[width=0.32\textwidth]{mesh_2Dsttri.png}}
  \subfloat{\includegraphics[width=0.32\textwidth]{mesh_2Duntri.png}}
  \subfloat{\includegraphics[width=0.32\textwidth]{mesh_3Dtets.png}}\\
  \subfloat{\includegraphics[width=0.32\textwidth]{mesh_2Dstquad.png}}
  \subfloat{\includegraphics[width=0.32\textwidth]{mesh_2Dunquad.png}}
  \subfloat{\includegraphics[width=0.32\textwidth]{mesh_3Dhexes.png}}
  \caption{The structured and unstructured grids considered in this paper.\label{Fig:meshes}}
\end{figure}
\subsection{Software and solver implementation}
In the next section, four different test problems are considered to demonstrate
the unique capabilities of the TAS spectrum analysis. For the first three problems,
the following sophisticated finite element software packages are examined: the 
C++ implementation of the FEniCS/Dolfin Project (Docker tag: 2017.1.0.r1), the 
the Python implementation of the FiredrakeProject \cite{florian_rathgeber_2018_1189452,
david_a_ham_2017_1022065,fabio_luporini_2017_836678,miklos_homolya_2018_1189458,
lawrence_mitchell_2018_1135096,miklos_homolya_2018_1189448,anders_logg_2018_1189453,
barry_smith_2017_1022071,lisandro_dalcin_2017_1022068},
and the C++ implementation of the deal.II library (Docker tag: v8.5.1-gcc-mpi-fulldepscandi-debugrelease). 
All three packages use various versions of PETSc for the solution of linear and nonlinear systems. 
The last problem utilizes the development version of PETSc (SHA1: v3.8.3-1636-gbcc3281268)
and its native finite element framework. The FEniCS/Dolfin/Firedrake software calculate 
the $L_2$ error norms by projecting the analytical solution $u$ onto a function space 3 degrees
order higher than the finite element solution $u_h$, whereas both deal.II and PETSc do the 
integral directly and use the same function space for both $u$ and $u_h$.

The test problems will be tested on the various meshes 
depicted in Figure~\ref{Fig:meshes}.  
The FEniCS/Dolfin and deal.II libraries use custom meshing, while 
Firedrake uses PETSc to manage unstructured meshes~
\cite{KnepleyKarpeev09,LangeMitchellKnepleyGorman2015,LangeKnepleyGorman2015},
and the hexahedral meshes are generated using the algorithms
described in~\cite{Bercea2016,McRae2016,Homolya2016}.
All timings will simply measure the finite element assembly 
and solve steps but not the mesh generation or any other 
preprocessing steps. 

The first three test problems presented in this paper will be 
solved using the conjugate gradient 
method with HYPRE's algebraic multigrid solver, BoomerAMG~\cite{falgout2002hypre}, and have 
a relative convergence criterion of $10^{-7}$. The last
problem will also consider two other multigrid solvers---the PETSc-native GAMG
preconditioner~\cite{adams2002evaluation,Adams-04} and Trilinos' Multi-Level (ML) solver~\cite{ml_users_guide}---but have a relative
convergence criterion of $10^{-9}$.

The first three tests are conducted on a single 3.5 GHz 
Quad-core Intel Core i5-7600 processor with 64 GB of 2400 MHz DDR4 memory.
The two (2D) problems are run in serial whereas the third (3D) problem is run across 4 MPI processes. 
The last problem is conducted on the Cori Cray XC40 system at the National Energy Research Scientific Computing Center (NERSC), 
and utilizes 32 Intel Xeon E5-2698v3 (``Haswell'') nodes for a total of 1024 MPI processes.
% RTM: I removed the registered trademark symbols above. Almost all journal style guidelines specify to not use them.
% Plus, we're not Intel and thus have no legal need to use them.
