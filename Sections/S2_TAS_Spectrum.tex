\section{TAS Spectrum}\label{sec:spectrum}
In order to incorporate accuracy into our performance analysis, 
we must first have an idea of convergence, or alternatively the numerical 
accuracy of a solution. In this paper, we will measure
solution accuracy using the $L_2$ norm of the error $err$,
\begin{align}
  err  = \left\lVert u_h - u \right\rVert_{L_2},
\end{align}
where $u_h$ is the finite element solution, $u$ is the exact solution,
and $h$ is some measure of our resolution such as the longest edge in any mesh element. 
Convergence means that our error shrinks as we increase our resolution, 
so that $\lim_{h\to0} err = 0$. In fact, we expect most discretization methods 
to have a relation of the form
\begin{align}
 err \le C h^\alpha,
\end{align}
where $C$ is some constant and $\alpha$ is called the \textit{convergence rate} of the method. 
This relation can be verified by plotting the logarithm of the resolution $h$ against the negative 
logarithm of the error $err$, which we call the \textit{digits of accuracy}
(DoA),
\begin{align}
  \mathrm{DoA} = -\log_{10} err.
\end{align}
This should produce a line with slope $-\alpha$, and is typically called a \textit{mesh convergence}
diagram~\cite{BrennerScott02,Knepley2017}. If we use the number of unknowns $N$ (or DoF) instead of the 
resolution $h$, the slope of the line will be modified. For most schemes $N = D h^{-d}$, where $d$ 
is the spatial dimension and $D$ is a constant, so that the slope would become $\alpha/d$. In 
our development, we will use this form of convergence diagram and call $\log_{10} N$ the 
\textit{digits of size} (DoS). Much as weak-scaling explains the behavior of algorithm on a 
range of problem sizes, the mesh-convergence diagram explains the behavior of a discretization.

% Efficacy
{\color{red}Imitating the static-scaling analysis, we would like to examine the rate of accuracy production, namely the number of
accurate digits of the solution produced per unit time. However, this measure drops precipitously as the computation
proceeds. Thus, we will introduce a measure called efficacy, defined to be error multiplied by time, and the
\textit{digits of efficacy} (DoE) as its logarithm. Smaller efficacy is desirable, as this means either smaller error or
smaller time.} As shown in Section~\ref{sec:theory}, this rate has a linear dependence on problem size, and slope $d -
\alpha$. Our \textit{accuracy \color{red}rate} analysis plots digits of efficacy against time. This analysis will be
able to compare not only different parallel algorithms and algebraic solvers, but also discretizations, as demonstrated
in Section~\ref{sec:results}.

{\color{red}The efficacy measure can be understood in analogy with the \textit{action} of a mechanical system, defined as the
product of energy and time. The evolution of a classical mechanical systems minimizes the action over its
trajectory. Efficacy represents an analogous action for computation, and we conjecture that an optimal algorithm
minimizes this measure over its runtime.}

%-------------------;
%  TAS-description  ;
%-------------------;
\begin{figure}[t]
\centering
\subfloat{\includegraphics[width=0.95\textwidth]{tas-description.pdf}}
\caption{Pictorial description of the Time-Accuracy-Size (TAS) spectrum.\label{Fig:tasspectrum}}
\end{figure}

We now present the Time-Accuracy-Size (TAS) spectrum. In 
Figure~\ref{Fig:tasspectrum}, we show the relation of our new 
efficacy analysis to the existing mesh convergence and
static scaling plots. As outlined in 
\cite{ChangNakshatralaKnepleyJohnsson2017}, static-scaling
measures the degrees of freedom solved per second for a given 
parallelism. That is,
\begin{align}\label{eqn:dofpersec}
  \mbox{Static-scaling} \qquad\mathrm{measures}\qquad \left(\frac{\mathrm{size}/\mathrm{time}}{\mathrm{time}}\right)
\end{align}
We assume that the problems are of linear complexity, i.e.\ the time is in
$\mathcal{O}(N)$, so optimal scaling is indicated by a horizontal line 
as the problem size is increased. A higher computation rate indicates
that the algorithm matches the hardware well, but tells us little about
how accurate the solution is. Measures of the accuracy as a function of problem size,
however, are basic to numerical analysis, and usually referred to as mesh convergence,
\begin{align}\label{eqn:doaperdos}
  \mbox{Mesh-convergence} \qquad\mathrm{measures}\qquad \left(\frac{1/\mathrm{error}}{\mathrm{size}}\right)
\end{align}
where we use the inverse of error since we usually measure the negative logarithm of the error. Multiplying equations
\eqref{eqn:dofpersec} and \eqref{eqn:doaperdos} together, we arrive at rate for accuracy production,
\begin{align}\label{eqn:doapertime}
  \mbox{Accuracy rate} \qquad\mathrm{measures}\qquad
  \left(\frac{1/\mathrm{error}}{\mathrm{size}}\right)\times\left(\frac{\mathrm{size}/\mathrm{time}}{\mathrm{time}}\right) = \frac{1/(\mathrm{error}\times \mathrm{time})}{\mathrm{time}},
\end{align}
{\color{red}which are exactly the measures used in our accuracy rate plot, namely efficacy against time.} An alternate derivation
would be to scale the DoF count used in the typical static-scaling analysis by the mesh convergence ratio, which we call
\emph{true} static-scaling. This produces the same measure, but slightly different scaling when logarithms are
applied. Looking at equations~\eqref{eqn:dofpersec}, \eqref{eqn:doaperdos}, and \eqref{eqn:doapertime} give us the TAS
spectrum, as visually depicted in Figure~\ref{Fig:tasspectrum}. This figure illustrates how the new efficacy analysis
can be applied to the existing mesh convergence and static scaling plots.

%-------------------;
%  Accuracy rates  ;
%-------------------;
\begin{figure}[t]
\centering
\subfloat{\includegraphics[width=0.4\textwidth]{DoEvsTrueDoFsec.pdf}}
\caption{Interpretation of both the DoE and true DoF per second metrics of a 
particular algorithm. DoE is often an indicator of how accurate an algorithm is
for a given amount of time, whereas true DoF per time is often
an indicator of how fast an algorithm processes each DoF when each DoF is
scaled by how much accuracy it contributes.\label{Fig:doevstruedofsec}}
\end{figure}
\subsection{Interpreting the TAS spectrum}
The complete TAS spectrum could potentially have three or four different diagrams that provide a wealth of 
performance information. We now show the recommended order of interpretation of these diagrams as well as 
provide some guidelines on how to synthesize the data into an understandable
framework.
\begin{enumerate}
\item \textsf{Mesh convergence}: This diagram not only shows whether the actual $L_2$ convergence 
matches the predicted $\alpha$, but how much accuracy is attained for a given size. Any tailing off that occurs 
in the line plots could potentially be an issue of solver tolerance or implementation errors. Such tail-offs will 
drastically affect both accuracy rate diagrams, so this diagram could be an early warning sign for unexpected
behavior in those plots. Furthermore, the mesh convergence ratio (i.e., the DoA over DoS) can also be an early predictor
as to which discretizations or implementations will have better accuracy rates.
\item \textsf{Static-scaling}: This particular scaling analysis is particularly useful for examining both strong-scaling and
weak-scaling limits of parallel finite element simulations across various hardware architectures and 
software/solver implementations. Optimal scaling would produce a horizontal line or a ``sweet spot'' assuming that the
algorithm is of $\mathcal{O}(N)$ complexity. Any tailing off in these static-scaling plots will have a 
direct affect on the accuracy rate plots.
\item \textsf{DoE}: This metric gives the simplest interpretation of numerical accuracy and computational cost.
A high DoE is most desired, and if straight lines are observed in both the mesh convergence and static-scaling 
diagrams, the lines in this diagram should exhibit some predicted slope which will be discussed in the next section. 
Note that the size of the problem is not explicitly taken into account in these diagrams; these diagrams 
{\color{red}provide an easy visual guide to the relative rate of accuracy production for different software
implementations or finite element discretizations.}
\item \textsf{True static-scaling}: Optionally, the ordering shown in the DoE diagrams can be further verified 
through the true static-scaling plots. The information provided by this analysis simply tells us how fast the algorithm is being computed assuming that all DoF are given equal weighting. Figure \ref{Fig:doevstruedofsec} provides a simple guideline on how to simultaneously interpret both the DoE and true static-scaling diagrams. Note that the true static-scaling diagrams will not always produce a horizontal line, as the DoF is now scaled by the DoA over DoS ratio.
\end{enumerate}



