\section{Introduction}
Computational scientists help bridge the gap between theory 
and application by translating mathematical techniques into robust software. One of
the most popular approaches undertaken is the development of sophisticated 
finite element packages like FEniCS/Dolfin~\cite{Logg2009,alnaes2015fenics}, deal.II~\cite{bangerth2016deal,AgelekAndersonBangerthBarth2017}, 
Firedrake~\cite{rathgeber2016firedrake}, LibMesh~\cite{libMeshPaper}, and 
MOOSE~\cite{Gaston2009} which provide application scientists the necessary
scientific tools to quickly address their specific needs. Alternatively, stand alone finite element
computational frameworks built on top of parallel linear algebra libraries like 
PETSc~\cite{petsc-user-ref,petsc-web-page} may need to be developed to 
address specific technical problems such as enforcing maximum principles 
in subsurface flow and transport modeling~\cite{Chang_JOMP_2017,
Chang_CMAME_2017,Mapakshi_JCP_2018} or modeling atmospheric and other geophysical
phenomena~\cite{brown2013icesheet,may_ptatin}, all of which could require field-scale 
or even global-scale resolutions. As scientific problems grow increasingly 
complex, the software and algorithms used must be fast, scalable, and 
efficient across a wide range of hardware architectures and scientific applications,
and new algorithms and numerical discretizations may need to be introduced. 
The ever increasing capacity and sophistication of processors, memory systems, 
and interconnects bring into question not only the performance of these new 
techniques, but their feasibility for large-scale problems. Specifically, how scalable is the software in both 
the algorithmic and parallel sense? Difficult problems require highly accurate numerical 
solutions, so it is desirable to take into consideration the solution accuracy 
along with both hardware utilization and algorithmic scalability. This paper is concerned with 
benchmarking the performance of various scientific software using analytic techniques.

\subsection{Overview of scaling analyses}
% Strong
% How does my solver handle one problem?
The most basic parallel scaling analysis, known as \textit{strong-scaling}, looks at the 
marginal efficiency of each additional processor working on a given problem~
\cite{Amdahl1967,Eijkhout2014,Knepley2017}. A series of experiments is
run using a fixed problem size but varying the number of processors used. It is typical to 
plot the number of processors $P$ against the speedup, defined as the time on one 
processor divided by the time on $P$ processors. Perfect speedup would result in a curve of 
unit positive slope. Because application scientists often have reason to solve a problem at a 
given resolution and spatial extent, strong-scaling is often of most interest: They may want to 
solve their problems in as little wall-clock time as possible, or, when running on 
shared resources, to complete a set of simulations in reasonable time without using 
too many CPU hours of their allocation (running in a strong-scaling ``sweet spot'' for 
their problem and machine). A simple strong-scaling analysis may, however, make it 
difficult to disentangle sources of inefficiency (serial sections,
communication, latency, algorithmic problems, etc.). 

% Weak
% How does my solver handle a range of problem sizes?
In some cases, application scientists may be interested in exploring a problem 
at a range of resolutions or spatial extents. This leads naturally to {\it weak-scaling}~
\cite{Gustafson1988,Eijkhout2014,Knepley2017} scenarios: 
Instead of fixing the global problem size, they fix the portion of the problem on each 
processor and scale the entire problem size linearly with the number of processors. 
It is typical to plot the number of processors $P$ against the efficiency, defined as 
the time on one processor over the time on $P$ processors. Perfect efficiency would 
result in a flat curve at unity. This analysis shows the marginal efficiency of adding another 
subproblem, rather than just a processor, and separates communication overhead and
algorithmic inefficiency from the problem of serial sections.

% Static
% How does my machine handle a range of problem sizes?
It is difficult to see, from either strong- or weak-scaling diagrams, how a given machine or 
algorithm will handle a variety of workloads. For example, is there is a minimum solution 
time where solver operations are swamped by latency? Is there a problem size where 
algorithmic pieces with suboptimal scaling start to dominate? We can examine these 
questions by running a series of problem sizes at fixed parallelism, called 
\textit{static-scaling}~\cite{Brown2016,ChangNakshatralaKnepleyJohnsson2017}. It is typical to 
plot the computation rate, in number of degrees-of-freedom (DoF) per time, against the time. 
Perfect scaling would result in a flat curve. Tailing off at small times is generally due to 
latency effects, and the curve will terminate at the smallest turnaround time for the 
machine. Decay at large times indicates suboptimal algorithmic
performance or suboptimal memory access patterns and cache misses for 
larger problems. Thus we can see both strong- and weak-scaling
effects on the same graph. It is also harder to game the result, since 
runtime is reported directly and extra work is directly visible. Static-scaling is a useful 
analytic technique for understanding the performance and scalability of
PDE solvers across different hardware architectures and software implementations.

From the standpoint of scientific computing, a significant drawback of all of the above 
types of analyses is that they treat all computation equally and do not consider the theoretical
convergence rate of the particular numerical discretization. The floating-point operations 
(FLOPs) done in the service of a quadratically convergent method, for instance, should 
be considered more valuable than those done for a linear method if both are in the basin of
convergence. These analyses do not depend at all on numerical accuracy so the actual 
convergence behavior is typically measured empirically using the Method of Manufactured 
Solutions (MMS).  If all equations are created equal, say for methods with roughly similar 
convergence behavior, then static-scaling is viable. However, it is insufficient 
when comparing methods with very different approximation properties. Any comparative 
study between different finite element methods or numerical
discretization should factor accuracy into the scaling analyses.

\subsection{Main contribution}
The aim of this paper is to present an alternative performance spectrum model which takes 
into account time-to-solution, accuracy of the numerical solution, and size of the problem hence
the Time-Accuracy-Size (TAS) spectrum analysis. These are the three metrics of most importance
when a comparative study involving different finite element or any numerical methods is needed.
Not every DoF has an equal contribution to the discretization's level of accuracy, so the DoF per 
time metric alone would be neither a fair nor accurate way of assessing the quality of a particular 
software's implementation of the finite element method. An outline of the salient features of this paper
are listed below:
\begin{itemize}
\item We provide a modification to the static-scaling analysis incorporating numerical accuracy.
\item We present the TAS spectrum and discuss how to analyze its diagrams.
\item Popular software packages, such as FEniCS/Dolfin, deal.II, Firedrake, and PETSc, are compared using the Poisson problem.
\item Different single-field finite element discretizations, such as the Galerkin and Discontinuous Galerkin methods, are also compared.
\item The analysis is extended to larger-scale computations, i.e.\ over 1K MPI processes.
\end{itemize}

The rest of the paper is organized as follows. In Section~\ref{sec:spectrum}, we 
present the framework of the Time-Accuracy-Size (TAS) spectrum model and outline 
how to interpret the diagrams. In Section~\ref{sec:theory}, we provide a theoretical 
derivation of the TAS spectrum and provide example convergence plots one may expect
to see. In Section~\ref{sec:setup}, we describe in detail how the finite element
experiments are setup. In Section~\ref{sec:results}, we demonstrate various
ways the TAS spectrum is useful by running various test cases. Conclusions 
and possible extensions of this work are outlined in 
Section~\ref{sec:conclusion}.
