SECTIONS = Sections/S2_TAS_Spectrum.tex Sections/S4_TAS_Setup.tex Sections/S5_TAS_Results.tex Sections/S1_TAS_Intro.tex Sections/S3_TAS_Theoretical.tex Sections/S6_TAS_Conclusion.tex

all: Driver_TAS.pdf

Driver_TAS.pdf: Driver_TAS.tex ${SECTIONS}
	BIBINPUTS=${BIBINPUTS}:${PETSC_DIR}/src/docs/tex latexmk -pdf Driver_TAS.tex
