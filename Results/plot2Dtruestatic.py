import numpy as np
import matplotlib.pyplot as plt
import sys

# Initial data
hscale = float(sys.argv[1])
D = 4
C = 10
W = 1

dof = D*100000*np.array([1,4,16,64,256,1024,4096,16384,65536,262144])
times = W*np.array([1.75,4.8,16.5,64,256,1024,4096,16384,70000,330000])
hsize = hscale*np.array([1/2,1/4,1/8,1/16,1/32,1/64,1/128,1/256,1/512,1/1024])
DoS = np.log10(D*np.power(hsize,-2))

alpha1 = -np.log10(C*np.power(hsize,2))
alpha2 = -np.log10(C*np.power(hsize,3))
alpha3 = -np.log10(C*np.power(hsize,4))
denom = DoS
fact1 = np.divide(alpha1,denom)
fact2 = np.divide(alpha2,denom)
fact3 = np.divide(alpha3,denom)

dofsec = np.divide(dof,times)
truedofsec1 = np.multiply(fact1,dofsec)
truedofsec2 = np.multiply(fact2,dofsec)
truedofsec3 = np.multiply(fact3,dofsec)
slope1 = np.polyfit(np.log10(times[2:6]),np.log10(truedofsec1[2:6]),1)
slope2 = np.polyfit(np.log10(times[2:6]),np.log10(truedofsec2[2:6]),1)
slope3 = np.polyfit(np.log10(times[2:6]),np.log10(truedofsec3[2:6]),1)
print(slope1)
print(slope2)
print(slope3)

# Plot doasec vs time
fig, ax = plt.subplots(figsize=(8,5))
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
ax.loglog(times,dofsec,linewidth=2,marker='o',color='red',label=r'$\mathrm{Standard\;DoF/}T$')
ax.loglog(times,truedofsec1,linewidth=2,marker='s',linestyle='--',color='black',label=r'$True\;\mathrm{DoF/}T\;(\alpha = 2)$')
ax.loglog(times,truedofsec2,linewidth=2,marker='v',linestyle='-.',color='black',label=r'$True\;\mathrm{DoF/}T\;(\alpha = 3)$')
ax.loglog(times,truedofsec3,linewidth=2,marker='x',linestyle=':',color='black',label=r'$True\;\mathrm{DoF/}T\;(\alpha = 4)$')
ax.set_ylabel(r'$\mathrm{DoF\;over}\;T$',fontsize=16)
ax.set_xlabel(r'$\mathrm{Normalized}\;T$',fontsize=16)
ax.set_xlim(1,1e6)
ax.set_ylim(1e5,1e6)
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
leg = ax.legend(loc='best',fontsize=14)
leg.get_frame().set_edgecolor('k')
plt.subplots_adjust(left=0.12,bottom=0.15,right=0.95,top=0.95,hspace=0.1)
plt.show()
