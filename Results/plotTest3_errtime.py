import numpy as np
import matplotlib.pyplot as plt

# Initial data
cg1time = np.loadtxt('data/cg1test3_time')
cg2time = np.loadtxt('data/cg2test3_time')
dg1time = np.loadtxt('data/dg1test3_time')
dg2time = np.loadtxt('data/dg2test3_time')
cg1error = np.loadtxt('data/cg1test3_error')
cg2error = np.loadtxt('data/cg2test3_error')
dg1error = np.loadtxt('data/dg1test3_error')
dg2error = np.loadtxt('data/dg2test3_error')

# Normalize time
cg1doasec = -np.log10(np.multiply(cg1error,cg1time))
cg2doasec = -np.log10(np.multiply(cg2error,cg2time))
dg1doasec = -np.log10(np.multiply(dg1error,dg1time))
dg2doasec = -np.log10(np.multiply(dg2error,dg2time))

# Plot doasec vs time
fig, ax = plt.subplots(figsize=(8,5))
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
ax.semilogx(cg1time,cg1doasec,linewidth=2,marker='o',color='blue',label='CG1')
ax.semilogx(cg2time,cg2doasec,linewidth=2,marker='<',color='darkmagenta',label='CG2')
ax.semilogx(dg1time,dg1doasec,linewidth=2,marker='s',color='red',label='DG1')
ax.semilogx(dg2time,dg2doasec,linewidth=2,marker='D',color='brown',label='DG2')
ax.set_ylabel('DoE',fontsize=16)
ax.set_xlabel('Time (s)',fontsize=16)
ax.set_xlim(1e-1,3e2)
ax.set_ylim(0,6)
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
leg = ax.legend(loc='best',fontsize=14)
leg.get_frame().set_edgecolor('k')
plt.subplots_adjust(left=0.12,bottom=0.15,right=0.95,top=0.95,hspace=0.1)
plt.show()
