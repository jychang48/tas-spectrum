import numpy as np
import matplotlib.pyplot as plt

# Initial data
festoveralltime = np.loadtxt('data/fetest2st_time')
fdstoveralltime = np.loadtxt('data/fdtest2st_time')
fdstqoveralltime = np.loadtxt('data/fdtest2stquad_time')
destoveralltime = np.loadtxt('data/detest2st_time')
festassembletime = np.loadtxt('data/fetest2st_timeassemble')
fdstassembletime = np.loadtxt('data/fdtest2st_timeassemble')
fdstqassembletime = np.loadtxt('data/fdtest2stquad_timeassemble')
destassembletime = np.loadtxt('data/detest2st_timeassemble')
fdstqdof = np.loadtxt('data/fdtest2stquad_dof')
festtime = festoveralltime - festassembletime
fdsttime = fdstoveralltime - fdstassembletime
fdstqtime = fdstqoveralltime - fdstqassembletime
desttime = destoveralltime - destassembletime
festdofsec = np.divide(fdstqdof,festtime)
fdstdofsec = np.divide(fdstqdof,fdsttime)
fdstqdofsec = np.divide(fdstqdof,fdstqtime)
destdofsec = np.divide(fdstqdof,desttime)

print(fdstqassembletime)
print(fdstqoveralltime)
print(fdstqtime)
# Plot dofsec vs time
fig, ax = plt.subplots(figsize=(8,5))
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle='')
ax.loglog(festtime,festdofsec,linewidth=2,marker='^',color='darkgreen',label='FEniCS - Triangles')
ax.loglog(fdsttime,fdstdofsec,linestyle='--',linewidth=2,marker='v',color='navy',label='Firedrake - Triangles')
ax.loglog(fdstqtime,fdstqdofsec,linestyle='-.',linewidth=2,marker='<',color='darkturquoise',label='Firedrake - Quads')
ax.loglog(desttime,destdofsec,linestyle=':',linewidth=2,marker='>',color='olive',label='deal.II - Quads')
ax.set_ylabel('DoF/s',fontsize=16)
ax.set_xlabel('Time (s)',fontsize=16)
ax.set_xlim(1e-4,2e0)
ax.set_ylim(1000,2000000)
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
leg = ax.legend(loc='lower left',fontsize=14)
leg.get_frame().set_edgecolor('k')
plt.subplots_adjust(left=.12,bottom=.15,right=.95,top=.95,hspace=.1)
plt.show()
