import numpy as np
import matplotlib.pyplot as plt

# Initial data
C = 10
W = 0.1
d = 3
CW = np.log10(C*W)

times = np.array([1.75,4.8,16.5,64,256,1024,4096,16384,70000,330000])
hsize = 0.2*np.array([1/2,1/4,1/8,1/16,1/32,1/64,1/128,1/256,1/512,1/1024])

DoE1 = (d - 2)*np.log10(hsize)-CW
DoE2 = (d - 3)*np.log10(hsize)-CW
DoE3 = (d - 4)*np.log10(hsize)-CW

# Plot doe vs time
fig, ax = plt.subplots(figsize=(8,5))
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
ax.semilogx(times,DoE1,linewidth=2,marker='s',linestyle='--',color='black',label=r'$\mathrm{DoE}\;(\alpha = 2)$')
ax.semilogx(times,DoE2,linewidth=2,marker='v',linestyle='-.',color='black',label=r'$\mathrm{DoE}\;(\alpha = 3)$')
ax.semilogx(times,DoE3,linewidth=2,marker='x',linestyle=':',color='black',label=r'$\mathrm{DoE}\;(\alpha = 4)$')
ax.set_ylabel(r'$\mathrm{DoE}$',fontsize=16)
ax.set_xlabel(r'$\mathrm{Normalized}\;T$',fontsize=16)
ax.set_xlim(1,1e6)
ax.set_ylim(-4,8)
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
leg = ax.legend(loc='best',fontsize=14)
leg.get_frame().set_edgecolor('k')
plt.subplots_adjust(left=0.12,bottom=0.15,right=0.95,top=0.95,hspace=0.1)
plt.show()
