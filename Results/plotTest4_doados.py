import numpy as np
import matplotlib.pyplot as plt


# Initial data
gamgcg1dof = np.loadtxt('data/Test4cg1gamg_dof')
hyprecg1dof = np.loadtxt('data/Test4cg1hypre_dof')
mlcg1dof = np.loadtxt('data/Test4cg1ml_dof')
gamgcg2dof = np.loadtxt('data/Test4cg2gamg_dof')
hyprecg2dof = np.loadtxt('data/Test4cg2hypre_dof')
mlcg2dof = np.loadtxt('data/Test4cg2ml_dof')
gamgcg1error = np.loadtxt('data/Test4cg1gamg_error')
hyprecg1error = np.loadtxt('data/Test4cg1hypre_error')
mlcg1error = np.loadtxt('data/Test4cg1ml_error')
gamgcg2error = np.loadtxt('data/Test4cg2gamg_error')
hyprecg2error = np.loadtxt('data/Test4cg2hypre_error')
mlcg2error = np.loadtxt('data/Test4cg2ml_error')
gamgcg1time = np.loadtxt('data/Test4cg1gamg_time')
hyprecg1time = np.loadtxt('data/Test4cg1hypre_time')
mlcg1time = np.loadtxt('data/Test4cg1ml_time')
gamgcg2time = np.loadtxt('data/Test4cg2gamg_time')
hyprecg2time = np.loadtxt('data/Test4cg2hypre_time')
mlcg2time = np.loadtxt('data/Test4cg2ml_time')
gamgcg1doa = -np.log10(gamgcg1error)
hyprecg1doa = -np.log10(hyprecg1error)
mlcg1doa = -np.log10(mlcg1error)
gamgcg2doa = -np.log10(gamgcg2error)
hyprecg2doa = -np.log10(hyprecg2error)
mlcg2doa = -np.log10(mlcg2error)
gamgcg1dos = np.log10(gamgcg1dof)
hyprecg1dos = np.log10(hyprecg1dof)
mlcg1dos = np.log10(mlcg1dof)
gamgcg2dos = np.log10(gamgcg2dof)
hyprecg2dos = np.log10(hyprecg2dof)
mlcg2dos = np.log10(mlcg2dof)

# Plot doa vs dos
fig, ax = plt.subplots(figsize=(8,5))
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
ax.plot(gamgcg1dos,gamgcg1doa,linewidth=2,marker='o',color='darkorange',label='CG1 - GAMG')
ax.plot(hyprecg1dos,hyprecg1doa,linewidth=2,linestyle='-',marker='s',color='darkkhaki',label='CG1 - HYPRE')
ax.plot(mlcg1dos,mlcg1doa,linewidth=2,linestyle='-',marker='D',color='darkolivegreen',label='CG1 - ML')
ax.plot(gamgcg2dos,gamgcg2doa,linewidth=2,marker='v',color='m',label='CG2 - GAMG')
ax.plot(hyprecg2dos,hyprecg2doa,linewidth=2,linestyle='-',marker='^',color='darkmagenta',label='CG2 - HYPRE')
ax.plot(mlcg2dos,mlcg2doa,linewidth=2,linestyle='-',marker='<',color='slateblue',label='CG2 - ML')
ax.set_ylabel('DoA',fontsize=16)
ax.set_xlabel('DoS',fontsize=16)
ax.set_xlim(0,9)
ax.set_ylim(0,6)
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
leg = ax.legend(loc='best',fontsize=14)
leg.get_frame().set_edgecolor('k')
plt.subplots_adjust(left=0.12,bottom=0.15,right=0.95,top=0.95,hspace=0.1)
plt.show()
