import numpy as np
import matplotlib.pyplot as plt

# Initial data
festdof = np.loadtxt('data/fetest2st_dof')
fdstdof = np.loadtxt('data/fdtest2st_dof')
fdstqdof = np.loadtxt('data/fdtest2stquad_dof')
festdoa = np.loadtxt('data/fetest2st_doa')
fdstdoa = np.loadtxt('data/fdtest2st_doa')
fdstqdoa = np.loadtxt('data/fdtest2stquad_doa')
destdoa = np.loadtxt('data/detest2st_doa')
festdos = np.log10(festdof)
fdstdos = np.log10(fdstdof)
fdstqdos = np.log10(fdstqdof)
destdos = fdstqdos

# Plot doa vs dos
fig, ax = plt.subplots(figsize=(5,5))
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
ax.plot(festdos,festdoa,linewidth=2,marker='^',color='darkgreen',label='FEniCS - Triangles')
ax.plot(fdstdos,fdstdoa,linestyle='--',linewidth=2,marker='v',color='navy',label='Firedrake - Triangles')
ax.plot(fdstqdos,fdstqdoa,linestyle='-.',linewidth=2,marker='<',color='darkturquoise',label='Firedrake - Quads')
ax.plot(destdos,destdoa,linestyle=':',linewidth=2,marker='>',color='olive',label='deal.II - Quads')
ax.set_ylabel('DoA',fontsize=16)
ax.set_xlabel('DoS',fontsize=16)
ax.set_xlim(0,6)
ax.set_ylim(0,6)
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
leg = ax.legend(loc='best',fontsize=14)
leg.get_frame().set_edgecolor('k')
plt.subplots_adjust(left=0.12,bottom=0.15,right=0.95,top=0.95,hspace=0.1)
plt.show()
