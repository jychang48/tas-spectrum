import numpy as np
import matplotlib.pyplot as plt

# Initial data
hexcg1time = np.loadtxt('data/hexcg1test3_time')
hexcg2time = np.loadtxt('data/hexcg2test3_time')
hexdg1time = np.loadtxt('data/hexdg1test3_time')
hexdg2time = np.loadtxt('data/hexdg2test3_time')
hexcg1error = np.loadtxt('data/hexcg1test3_error')
hexcg2error = np.loadtxt('data/hexcg2test3_error')
hexdg1error = np.loadtxt('data/hexdg1test3_error')
hexdg2error = np.loadtxt('data/hexdg2test3_error')

# Normalize time
hexcg1doasec = -np.log10(np.multiply(hexcg1error,hexcg1time))
hexcg2doasec = -np.log10(np.multiply(hexcg2error,hexcg2time))
hexdg1doasec = -np.log10(np.multiply(hexdg1error,hexdg1time))
hexdg2doasec = -np.log10(np.multiply(hexdg2error,hexdg2time))

# Plot doasec vs time
fig, ax = plt.subplots(figsize=(8,5))
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
ax.semilogx(hexcg1time,hexcg1doasec,linewidth=2,marker='o',color='blue',label='CG1')
ax.semilogx(hexcg2time,hexcg2doasec,linewidth=2,marker='<',color='darkmagenta',label='CG2')
ax.semilogx(hexdg1time,hexdg1doasec,linewidth=2,marker='s',color='red',label='DG1')
ax.semilogx(hexdg2time,hexdg2doasec,linewidth=2,marker='D',color='brown',label='DG2')
ax.set_ylabel('DoE',fontsize=16)
ax.set_xlabel('Time (s)',fontsize=16)
ax.set_xlim(1e-1,3e2)
ax.set_ylim(0,6)
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
leg = ax.legend(loc='best',fontsize=14)
leg.get_frame().set_edgecolor('k')
plt.subplots_adjust(left=0.12,bottom=0.15,right=0.95,top=0.95,hspace=0.1)
plt.show()
