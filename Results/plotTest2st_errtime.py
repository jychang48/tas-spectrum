import numpy as np
import matplotlib.pyplot as plt

# Initial data
festtime = np.loadtxt('data/fetest2st_time')
fdsttime = np.loadtxt('data/fdtest2st_time')
fdstqtime = np.loadtxt('data/fdtest2stquad_time')
desttime = np.loadtxt('data/detest2st_time')
festerror = np.loadtxt('data/fetest2st_error')
fdsterror = np.loadtxt('data/fdtest2st_error')
fdstqerror = np.loadtxt('data/fdtest2stquad_error')
destdoa = np.loadtxt('data/detest2st_doa')
desterror = np.power(10,-destdoa)

festlograte = -np.log10(np.multiply(festerror,festtime))
fdstlograte = -np.log10(np.multiply(fdsterror,fdsttime))
fdstqlograte = -np.log10(np.multiply(fdstqerror,fdstqtime))
destlograte = -np.log10(np.multiply(desterror,desttime))

# Plot dofsec vs time
fig, ax = plt.subplots(figsize=(8,5))
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle='')
ax.semilogx(festtime,festlograte,linewidth=2,marker='^',color='darkgreen',label='FEniCS - Triangles')
ax.semilogx(fdsttime,fdstlograte,linestyle='--',linewidth=2,marker='v',color='navy',label='Firedrake - Triangles')
ax.semilogx(fdstqtime,fdstqlograte,linestyle='-.',linewidth=2,marker='<',color='darkturquoise',label='Firedrake - Quads')
ax.semilogx(desttime,destlograte,linestyle=':',linewidth=2,marker='>',color='olive',label='deal.II - Quads')
ax.set_ylabel('DoE',fontsize=16)
ax.set_xlabel('Time (s)',fontsize=16)
ax.set_xlim(1e-4,2e0)
ax.set_ylim(2,6)
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
leg = ax.legend(loc='lower left',fontsize=14)
leg.get_frame().set_edgecolor('k')
plt.subplots_adjust(left=.12,bottom=.15,right=.95,top=.95,hspace=.1)
plt.show()
