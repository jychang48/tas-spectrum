import numpy as np
import matplotlib.pyplot as plt


# Initial data
cg1dos = np.loadtxt('data/decg1_dos')
cg2dos = np.loadtxt('data/decg2_dos')
cg3dos = np.loadtxt('data/decg3_dos')
dg1dos = np.loadtxt('data/dedg1_dos')
dg2dos = np.loadtxt('data/dedg2_dos')
dg3dos = np.loadtxt('data/dedg3_dos')
cg1doa = np.loadtxt('data/decg1_doa')
cg2doa = np.loadtxt('data/decg2_doa')
cg3doa = np.loadtxt('data/decg3_doa')
dg1doa = np.loadtxt('data/dedg1_doa')
dg2doa = np.loadtxt('data/dedg2_doa')
dg3doa = np.loadtxt('data/dedg3_doa')

# Plot doa vs dos
fig, ax = plt.subplots(figsize=(5,5))
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
ax.plot(cg1dos[0:5],cg1doa[0:5],linewidth=2,marker='o',color='blue',label='CG1')
ax.plot(cg2dos[0:5],cg2doa[0:5],linewidth=2,marker='<',color='darkmagenta',label='CG2')
ax.plot(cg3dos[0:5],cg3doa[0:5],linewidth=2,marker='^',color='dodgerblue',label='CG3')
ax.plot(dg1dos[0:5],dg1doa[0:5],linewidth=2,marker='s',color='red',label='DG1')
ax.plot(dg2dos[0:5],dg2doa[0:5],linewidth=2,marker='D',color='brown',label='DG2')
ax.plot(dg3dos[0:5],dg3doa,linewidth=2,marker='v',color='darkorange',label='DG3')
ax.set_ylabel('DoA',fontsize=16)
ax.set_xlabel('DoS',fontsize=16)
ax.set_xlim(0,9)
ax.set_ylim(0,9)
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
leg = ax.legend(loc='best',fontsize=14)
leg.get_frame().set_edgecolor('k')
plt.subplots_adjust(left=0.12,bottom=0.15,right=0.95,top=0.95,hspace=0.1)
plt.show()
