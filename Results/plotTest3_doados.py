import numpy as np
import matplotlib.pyplot as plt


# Initial data
cg1dof = np.loadtxt('data/cg1test3_dof')
cg2dof = np.loadtxt('data/cg2test3_dof')
dg1dof = np.loadtxt('data/dg1test3_dof')
dg2dof = np.loadtxt('data/dg2test3_dof')
cg1doa = np.loadtxt('data/cg1test3_doa')
cg2doa = np.loadtxt('data/cg2test3_doa')
dg1doa = np.loadtxt('data/dg1test3_doa')
dg2doa = np.loadtxt('data/dg2test3_doa')
cg1dos = np.log10(cg1dof)
cg2dos = np.log10(cg2dof)
dg1dos = np.log10(dg1dof)
dg2dos = np.log10(dg2dof)

# Plot doa vs dos
fig, ax = plt.subplots(figsize=(5,5))
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
ax.plot(cg1dos,cg1doa,linewidth=2,marker='o',color='blue',label='CG1')
ax.plot(cg2dos,cg2doa,linewidth=2,marker='<',color='darkmagenta',label='CG2')
ax.plot(dg1dos,dg1doa,linewidth=2,marker='s',color='red',label='DG1')
ax.plot(dg2dos,dg2doa,linewidth=2,marker='D',color='brown',label='DG2')
ax.set_ylabel('DoA',fontsize=16)
ax.set_xlabel('DoS',fontsize=16)
ax.set_xlim(0,7)
ax.set_ylim(0,7)
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
leg = ax.legend(loc='best',fontsize=14)
leg.get_frame().set_edgecolor('k')
plt.subplots_adjust(left=0.12,bottom=0.15,right=0.95,top=0.95,hspace=0.1)
plt.show()
