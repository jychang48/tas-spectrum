import numpy as np
import matplotlib.pyplot as plt

# Initial data
feuntime = np.loadtxt('data/fetest2uh_time')
fduntime = np.loadtxt('data/fdtest2un_time')
fdunqtime = np.loadtxt('data/fdtest2unquad_time')
deuntime = np.loadtxt('data/detest2un_time')
feundoa = np.loadtxt('data/fetest2uh_doa')
fdundoa = np.loadtxt('data/fdtest2un_doa')
fdunqdoa = np.loadtxt('data/fdtest2unquad_doa')
deundoa = np.loadtxt('data/detest2un_doa')
feundof = np.loadtxt('data/fetest2uh_dof')
fdundof = np.loadtxt('data/fdtest2un_dof')
fdunqdof = np.loadtxt('data/fdtest2unquad_dof')
deundof = fdunqdof

# Normalize time
feundos = np.log10(feundof)
fdundos = np.log10(fdundof)
fdunqdos = np.log10(fdunqdof)
deundos = np.log10(deundof)
feundoasec = np.divide(np.multiply(np.divide(feundoa,feundos),feundof),feuntime)
fdundoasec = np.divide(np.multiply(np.divide(fdundoa,fdundos),fdundof),fduntime)
fdunqdoasec = np.divide(np.multiply(np.divide(fdunqdoa,fdunqdos),fdunqdof),fdunqtime)
deundoasec = np.divide(np.multiply(np.divide(deundoa,deundos),deundof),deuntime)

# Plot doasec vs time
fig, ax = plt.subplots(figsize=(8,5))
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle='')
ax.loglog(feuntime,feundoasec,linewidth=2,marker='^',color='darkgreen',label='FEniCS - Triangles')
ax.loglog(fduntime,fdundoasec,linestyle='--',linewidth=2,marker='v',color='navy',label='Firedrake - Triangles')
ax.loglog(fdunqtime,fdunqdoasec,linestyle='-.',linewidth=2,marker='<',color='darkturquoise',label='Firedrake - Quads')
ax.loglog(deuntime,deundoasec,linestyle=':',linewidth=2,marker='>',color='olive',label='deal.II - Quads')
ax.set_ylabel('True DoF/s',fontsize=16)
ax.set_xlabel('Time (s)',fontsize=16)
ax.set_xlim(1e-4,2e0)
ax.set_ylim(1e3,2e6)
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
leg = ax.legend(loc='lower left',fontsize=14)
leg.get_frame().set_edgecolor('k')
plt.subplots_adjust(left=.12,bottom=.15,right=.95,top=.95,hspace=.1)
plt.show()
