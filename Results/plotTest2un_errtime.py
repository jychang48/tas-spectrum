import numpy as np
import matplotlib.pyplot as plt

# Initial data
feuntime = np.loadtxt('data/fetest2uh_time')
fduntime = np.loadtxt('data/fdtest2un_time')
fdunqtime = np.loadtxt('data/fdtest2stquad_time')
deuntime = np.loadtxt('data/detest2un_time')
feunerror = np.loadtxt('data/fetest2uh_error')
fdunerror = np.loadtxt('data/fdtest2un_error')
fdunqerror = np.loadtxt('data/fdtest2stquad_error')
deundoa = np.loadtxt('data/detest2un_doa')
deunerror = np.power(10,-deundoa)

feunlograte = -np.log10(np.multiply(feunerror,feuntime))
fdunlograte = -np.log10(np.multiply(fdunerror,fduntime))
fdunqlograte = -np.log10(np.multiply(fdunqerror,fdunqtime))
deunlograte = -np.log10(np.multiply(deunerror,deuntime))

# Plot dofsec vs time
fig, ax = plt.subplots(figsize=(8,5))
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle='')
ax.semilogx(feuntime,feunlograte,linewidth=2,marker='^',color='darkgreen',label='FEniCS - Triangles')
ax.semilogx(fduntime,fdunlograte,linestyle='--',linewidth=2,marker='v',color='navy',label='Firedrake - Triangles')
ax.semilogx(fdunqtime,fdunqlograte,linestyle='-.',linewidth=2,marker='<',color='darkturquoise',label='Firedrake - Quads')
ax.semilogx(deuntime,deunlograte,linestyle=':',linewidth=2,marker='>',color='olive',label='deal.II - Quads')
ax.set_ylabel('DoE',fontsize=16)
ax.set_xlabel('Time (s)',fontsize=16)
ax.set_xlim(1e-4,2e0)
ax.set_ylim(2,6)
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
leg = ax.legend(loc='lower left',fontsize=14)
leg.get_frame().set_edgecolor('k')
plt.subplots_adjust(left=.12,bottom=.15,right=.95,top=.95,hspace=.1)
plt.show()
