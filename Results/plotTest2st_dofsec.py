import numpy as np
import matplotlib.pyplot as plt

# Initial data
festtime = np.loadtxt('data/fetest2st_time')
fdsttime = np.loadtxt('data/fdtest2st_time')
fdstqtime = np.loadtxt('data/fdtest2stquad_time')
desttime = np.loadtxt('data/detest2st_time')
fdstqdof = np.loadtxt('data/fdtest2stquad_dof')
festdofsec = np.loadtxt('data/fetest2st_dofsec')
fdstdofsec = np.loadtxt('data/fdtest2st_dofsec')
fdstqdofsec = np.loadtxt('data/fdtest2stquad_dofsec')
destdofsec = np.divide(fdstqdof,desttime)

# Plot dofsec vs time
fig, ax = plt.subplots(figsize=(8,5))
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle='')
ax.loglog(festtime,festdofsec,linewidth=2,marker='^',color='darkgreen',label='FEniCS - Triangles')
ax.loglog(fdsttime,fdstdofsec,linestyle='--',linewidth=2,marker='v',color='navy',label='Firedrake - Triangles')
ax.loglog(fdstqtime,fdstqdofsec,linestyle='-.',linewidth=2,marker='<',color='darkturquoise',label='Firedrake - Quads')
ax.loglog(desttime,destdofsec,linestyle=':',linewidth=2,marker='>',color='olive',label='deal.II - Quads')
ax.set_ylabel('DoF/s',fontsize=16)
ax.set_xlabel('Time (s)',fontsize=16)
ax.set_xlim(1e-4,2e0)
ax.set_ylim(1000,2000000)
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
leg = ax.legend(loc='lower left',fontsize=14)
leg.get_frame().set_edgecolor('k')
plt.subplots_adjust(left=.12,bottom=.15,right=.95,top=.95,hspace=.1)
plt.show()
