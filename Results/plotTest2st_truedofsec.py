import numpy as np
import matplotlib.pyplot as plt

# Initial data
festtime = np.loadtxt('data/fetest2st_time')
fdsttime = np.loadtxt('data/fdtest2st_time')
fdstqtime = np.loadtxt('data/fdtest2stquad_time')
desttime = np.loadtxt('data/detest2st_time')
festdoa = np.loadtxt('data/fetest2st_doa')
fdstdoa = np.loadtxt('data/fdtest2st_doa')
fdstqdoa = np.loadtxt('data/fdtest2stquad_doa')
destdoa = np.loadtxt('data/detest2st_doa')
festdof = np.loadtxt('data/fetest2st_dof')
fdstdof = np.loadtxt('data/fdtest2st_dof')
fdstqdof = np.loadtxt('data/fdtest2stquad_dof')
destdof = fdstqdof

# Normalize time
festdos = np.log10(festdof)
fdstdos = np.log10(fdstdof)
fdstqdos = np.log10(fdstqdof)
destdos = np.log10(destdof)
festdoasec = np.divide(np.multiply(np.divide(festdoa,festdos),festdof),festtime)
fdstdoasec = np.divide(np.multiply(np.divide(fdstdoa,fdstdos),fdstdof),fdsttime)
fdstqdoasec = np.divide(np.multiply(np.divide(fdstqdoa,fdstqdos),fdstqdof),fdstqtime)
destdoasec = np.divide(np.multiply(np.divide(destdoa,destdos),destdof),desttime)

# Plot doasec vs time
fig, ax = plt.subplots(figsize=(8,5))
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle='')
ax.loglog(festtime,festdoasec,linewidth=2,marker='^',color='darkgreen',label='FEniCS - Triangles')
ax.loglog(fdsttime,fdstdoasec,linestyle='--',linewidth=2,marker='v',color='navy',label='Firedrake - Triangles')
ax.loglog(fdstqtime,fdstqdoasec,linestyle='-.',linewidth=2,marker='<',color='darkturquoise',label='Firedrake - Quads')
ax.loglog(desttime,destdoasec,linestyle=':',linewidth=2,marker='>',color='olive',label='deal.II - Quads')
ax.set_ylabel('True DoF/s',fontsize=16)
ax.set_xlabel('Time (s)',fontsize=16)
ax.set_xlim(1e-4,2e0)
ax.set_ylim(1e3,2e6)
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
leg = ax.legend(loc='lower left',fontsize=14)
leg.get_frame().set_edgecolor('k')
plt.subplots_adjust(left=.12,bottom=.15,right=.95,top=.95,hspace=.1)
plt.show()
