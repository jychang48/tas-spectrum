import numpy as np
import matplotlib.pyplot as plt

# Initial data
feuntime = np.loadtxt('data/fetest2uh_time')
fduntime = np.loadtxt('data/fdtest2un_time')
fdunqtime = np.loadtxt('data/fdtest2unquad_time')
deuntime = np.loadtxt('data/detest2un_time')
fdunqdof = np.loadtxt('data/fdtest2unquad_dof')
feundofsec = np.loadtxt('data/fetest2uh_dofsec')
fdundofsec = np.loadtxt('data/fdtest2un_dofsec')
fdunqdofsec = np.loadtxt('data/fdtest2unquad_dofsec')
deundofsec = np.divide(fdunqdof,deuntime)

# Plot dofsec vs time
fig, ax = plt.subplots(figsize=(8,5))
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle='')
ax.loglog(feuntime,feundofsec,linewidth=2,marker='^',color='darkgreen',label='FEniCS - Triangles')
ax.loglog(fduntime,fdundofsec,linestyle='--',linewidth=2,marker='v',color='navy',label='Firedrake - Triangles')
ax.loglog(fdunqtime,fdunqdofsec,linestyle='-.',linewidth=2,marker='<',color='darkturquoise',label='Firedrake - Quads')
ax.loglog(deuntime,deundofsec,linestyle=':',linewidth=2,marker='>',color='olive',label='deal.II - Quads')
ax.set_ylabel('DoF/s',fontsize=16)
ax.set_xlabel('Time (s)',fontsize=16)
ax.set_xlim(1e-4,2e0)
ax.set_ylim(1000,2000000)
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
leg = ax.legend(loc='lower left',fontsize=14)
leg.get_frame().set_edgecolor('k')
plt.subplots_adjust(left=.12,bottom=.15,right=.95,top=.95,hspace=.1)
plt.show()
