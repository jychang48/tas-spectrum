import numpy as np
import matplotlib.pyplot as plt

# Initial data
dof = 400000*np.array([1,4,16,64,256,1024,4096,16384,65536,262144])
times = np.array([1.75,4.8,16.5,64,256,1024,4096,16384,70000,330000])
dofsec = np.divide(dof,times)

# Plot doasec vs time
fig, ax = plt.subplots(figsize=(12,7.5))
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
ax.loglog(times,dofsec,linewidth=2,marker='o',color='red')
ax.set_ylabel('DoF over Time',fontsize=16)
ax.set_xlabel('Time',fontsize=16)
ax.set_xlim(1,1e6)
ax.set_ylim(1e5,1e6)
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
plt.subplots_adjust(left=0.12,bottom=0.15,right=0.95,top=0.95,hspace=0.1)
plt.show()
