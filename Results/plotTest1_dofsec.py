import numpy as np
import matplotlib.pyplot as plt


# Initial data
cg1time = np.loadtxt('data/cg1test1_time')
cg2time = np.loadtxt('data/cg2test1_time')
cg3time = np.loadtxt('data/cg3test1_time')
dg1time = np.loadtxt('data/dg1test1_time')
dg2time = np.loadtxt('data/dg2test1_time')
dg3time = np.loadtxt('data/dg3test1_time')
cg1dofsec = np.loadtxt('data/cg1test1_dofsec')
cg2dofsec = np.loadtxt('data/cg2test1_dofsec')
cg3dofsec = np.loadtxt('data/cg3test1_dofsec')
dg1dofsec = np.loadtxt('data/dg1test1_dofsec')
dg2dofsec = np.loadtxt('data/dg2test1_dofsec')
dg3dofsec = np.loadtxt('data/dg3test1_dofsec')

# Plot dofsec vs time 
fig, ax = plt.subplots(figsize=(8,5))
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
ax.loglog(cg1time[0:5],cg1dofsec[0:5],linewidth=2,marker='o',color='blue',label='CG1')
ax.loglog(cg2time[0:5],cg2dofsec[0:5],linewidth=2,marker='<',color='darkmagenta',label='CG2')
ax.loglog(cg3time[0:5],cg3dofsec[0:5],linewidth=2,marker='^',color='dodgerblue',label='CG3')
ax.loglog(dg1time[0:5],dg1dofsec[0:5],linewidth=2,marker='s',color='red',label='DG1')
ax.loglog(dg2time[0:5],dg2dofsec[0:5],linewidth=2,marker='D',color='brown',label='DG2')
ax.loglog(dg3time[0:5],dg3dofsec,linewidth=2,marker='v',color='darkorange',label='DG3')
ax.set_ylabel('DoF/s',fontsize=16)
ax.set_xlabel('Time (s)',fontsize=16)
ax.set_xlim(1e-3,3e1)
ax.set_ylim(10000,1000000)
ax.tick_params(axis='both',labelsize=14)
ax.grid(b=True,which='both', linestyle=':')
leg = ax.legend(loc='upper right',fontsize=14)
leg.get_frame().set_edgecolor('k')
plt.subplots_adjust(left=0.12,bottom=0.15,right=0.95,top=0.95,hspace=0.1)
plt.show()
